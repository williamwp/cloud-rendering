#!/bin/bash

start_time=$(date +%s)
sleep 1

ELF=" ./tpacf.x -i Datapnts.1,Randompnts.1,Randompnts.2,Randompnts.3,Randompnts.4,Randompnts.5,Randompnts.6,Randompnts.7,Randompnts.8,Randompnts.9,Randompnts.10,Randompnts.11,Randompnts.12,Randompnts.13,Randompnts.14,Randompnts.15,Randompnts.16,Randompnts.17,Randompnts.18,Randompnts.19,Randompnts.20,Randompnts.21,Randompnts.22,Randompnts.23,Randompnts.24,Randompnts.25,Randompnts.26,Randompnts.27,Randompnts.28,Randompnts.29,Randompnts.30,Randompnts.31,Randompnts.32,Randompnts.33,Randompnts.34,Randompnts.35,Randompnts.36,Randompnts.37,Randompnts.38,Randompnts.39,Randompnts.40,Randompnts.41,Randompnts.42,Randompnts.43,Randompnts.44,Randompnts.45,Randompnts.46,Randompnts.47,Randompnts.48,Randompnts.49,Randompnts.50,Randompnts.51,Randompnts.52,Randompnts.53,Randompnts.54,Randompnts.55,Randompnts.56,Randompnts.57,Randompnts.58,Randompnts.59,Randompnts.60,Randompnts.61,Randompnts.62,Randompnts.63,Randompnts.64,Randompnts.65,Randompnts.66,Randompnts.67,Randompnts.68,Randompnts.69,Randompnts.70,Randompnts.71,Randompnts.72,Randompnts.73,Randompnts.74,Randompnts.75,Randompnts.76,Randompnts.77,Randompnts.78,Randompnts.79,Randompnts.80,Randompnts.81,Randompnts.82,Randompnts.83,Randompnts.84,Randompnts.85,Randompnts.86,Randompnts.87,Randompnts.88,Randompnts.89,Randompnts.90,Randompnts.91,Randompnts.92,Randompnts.93,Randompnts.94,Randompnts.95,Randompnts.96,Randompnts.97,Randompnts.98,Randompnts.99,Randompnts.100 -o a.out -- -n 100 -p 487 "


sudo nvprof -m       all  --profile-child-processes  $ELF
#sudo nvprof -m          pcie_total_data_received  --profile-child-processes  $ELF

# begin to execute the nvprof metrics 
if false; then
sudo nvprof -m         l2_tex_write_transactions  --profile-child-processes  $ELF
sudo nvprof -m                     flop_count_hp  --profile-child-processes  $ELF
sudo nvprof -m                 flop_count_hp_add  --profile-child-processes  $ELF
sudo nvprof -m                 flop_count_hp_mul  --profile-child-processes  $ELF
sudo nvprof -m                 flop_count_hp_fma  --profile-child-processes  $ELF
sudo nvprof -m                        inst_fp_16  --profile-child-processes  $ELF
sudo nvprof -m                               ipc  --profile-child-processes  $ELF
sudo nvprof -m                        issued_ipc  --profile-child-processes  $ELF
sudo nvprof -m            issue_slot_utilization  --profile-child-processes  $ELF
sudo nvprof -m          eligible_warps_per_cycle  --profile-child-processes  $ELF
sudo nvprof -m                shared_utilization  --profile-child-processes  $ELF
sudo nvprof -m                    l2_utilization  --profile-child-processes  $ELF
sudo nvprof -m                   tex_utilization  --profile-child-processes  $ELF
sudo nvprof -m               ldst_fu_utilization  --profile-child-processes  $ELF
sudo nvprof -m                 cf_fu_utilization  --profile-child-processes  $ELF
sudo nvprof -m                tex_fu_utilization  --profile-child-processes  $ELF
sudo nvprof -m            special_fu_utilization  --profile-child-processes  $ELF
sudo nvprof -m     half_precision_fu_utilization  --profile-child-processes  $ELF
sudo nvprof -m   single_precision_fu_utilization  --profile-child-processes  $ELF
sudo nvprof -m   double_precision_fu_utilization  --profile-child-processes  $ELF
sudo nvprof -m                flop_hp_efficiency  --profile-child-processes  $ELF
sudo nvprof -m                flop_sp_efficiency  --profile-child-processes  $ELF
sudo nvprof -m                flop_dp_efficiency  --profile-child-processes  $ELF
sudo nvprof -m           sysmem_read_utilization  --profile-child-processes  $ELF
sudo nvprof -m          sysmem_write_utilization  --profile-child-processes  $ELF


sudo nvprof -m       pcie_total_data_transmitted  --profile-child-processes  $ELF
sudo nvprof -m          pcie_total_data_received  --profile-child-processes  $ELF


sudo nvprof -m        inst_executed_global_loads  --profile-child-processes  $ELF
sudo nvprof -m         inst_executed_local_loads  --profile-child-processes  $ELF
sudo nvprof -m        inst_executed_shared_loads  --profile-child-processes  $ELF
sudo nvprof -m       inst_executed_surface_loads  --profile-child-processes  $ELF
sudo nvprof -m       inst_executed_global_stores  --profile-child-processes  $ELF
sudo nvprof -m        inst_executed_local_stores  --profile-child-processes  $ELF
sudo nvprof -m       inst_executed_shared_stores  --profile-child-processes  $ELF
sudo nvprof -m      inst_executed_surface_stores  --profile-child-processes  $ELF
sudo nvprof -m      inst_executed_global_atomics  --profile-child-processes  $ELF
sudo nvprof -m   inst_executed_global_reductions  --profile-child-processes  $ELF
sudo nvprof -m     inst_executed_surface_atomics  --profile-child-processes  $ELF
sudo nvprof -m  inst_executed_surface_reductions  --profile-child-processes  $ELF
sudo nvprof -m      inst_executed_shared_atomics  --profile-child-processes  $ELF
sudo nvprof -m             inst_executed_tex_ops  --profile-child-processes  $ELF
sudo nvprof -m              l2_global_load_bytes  --profile-child-processes  $ELF
sudo nvprof -m               l2_local_load_bytes  --profile-child-processes  $ELF
sudo nvprof -m             l2_surface_load_bytes  --profile-child-processes  $ELF
sudo nvprof -m                   dram_read_bytes  --profile-child-processes  $ELF
sudo nvprof -m                  dram_write_bytes  --profile-child-processes  $ELF
sudo nvprof -m       l2_local_global_store_bytes  --profile-child-processes  $ELF
sudo nvprof -m         l2_global_reduction_bytes  --profile-child-processes  $ELF
sudo nvprof -m      l2_global_atomic_store_bytes  --profile-child-processes  $ELF
sudo nvprof -m            l2_surface_store_bytes  --profile-child-processes  $ELF
sudo nvprof -m        l2_surface_reduction_bytes  --profile-child-processes  $ELF
sudo nvprof -m     l2_surface_atomic_store_bytes  --profile-child-processes  $ELF
sudo nvprof -m              global_load_requests  --profile-child-processes  $ELF
sudo nvprof -m               local_load_requests  --profile-child-processes  $ELF
sudo nvprof -m             surface_load_requests  --profile-child-processes  $ELF
sudo nvprof -m             global_store_requests  --profile-child-processes  $ELF
sudo nvprof -m              local_store_requests  --profile-child-processes  $ELF
sudo nvprof -m            surface_store_requests  --profile-child-processes  $ELF
sudo nvprof -m            global_atomic_requests  --profile-child-processes  $ELF
sudo nvprof -m         global_reduction_requests  --profile-child-processes  $ELF
sudo nvprof -m           surface_atomic_requests  --profile-child-processes  $ELF
sudo nvprof -m        surface_reduction_requests  --profile-child-processes  $ELF
sudo nvprof -m                 sysmem_read_bytes  --profile-child-processes  $ELF
sudo nvprof -m                sysmem_write_bytes  --profile-child-processes  $ELF
sudo nvprof -m                   l2_tex_hit_rate  --profile-child-processes  $ELF
sudo nvprof -m             texture_load_requests  --profile-child-processes  $ELF
fi

end_time=$(date +%s)
cost_time=$[ $end_time-$start_time ]
echo "Cost time is $(($cost_time/60)) min $(($cost_time%60)) s"
