#!/usr/bin/python3
# coding=utf8

import csv

# csv.writer()方法返回一个 writer 对象，该对象将用户数据转换为给定文件状对象上的定界字符串。

numbers = [[1, 2, 3, 4], [5, 6, 7, 8] ]
# 该脚本将数字写入numbers2.csv文件。 writerow()方法将一行数据写入指定的文件

f = open('numbers.csv', 'w')

with f:

    writer = csv.writer(f)

    for row in numbers:

        writer.writerow(row)
