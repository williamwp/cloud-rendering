# keys函数是Python的字典函数，它返回字典中的所有键所组成的一个可迭代序列。使用keys()可以获得字典中所有的键。

# 语法dictionary.keys()

#!/usr/bin/python3

# list({'Chinasoft':'China','Microsoft':'USA'}.keys())

# >>> list({'Chinasoft':'China','Microsoft':'USA'}.keys())
# ['Chinasoft', 'Microsoft']
