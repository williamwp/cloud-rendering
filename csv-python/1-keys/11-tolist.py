#!/usr/bin/python3

import pandas as pd
import numpy as np


a = np.array([[1,2,3], [4,5,6], [7,8,9] ])


b = a.tolist()

print("a")
print(a)
print("----------------------- \n")

print("b")
print(b)
print("----------------------- \n \n ")


print("the length of b")
print(len(b))

print("b[0][0] is    ")
print(b[0][0])
print("----------------------- \n \n ")


