#!/usr/bin/python3
# coding=utf8

# Python 3有一个内置函数next()，它通过调用其next ()方法从迭代器中检索下一个项目。 如果给定了默认值，则在迭代器耗尽返回此默认值，否则会引发StopIteration。 该方法可用于从文件对象读取下一个输入行。

print ('1 内置函数next()')
numbers = [1, 2, 3, 4]
it = iter(numbers)

print (next(it))
print (next(it))
print (next(it))
print (next(it))
#print (next(it))
#     iterator − 要读取行的文件对象
#     default − 如果迭代器耗尽则返回此默认值。 如果没有给出此默认值，则抛出 StopIteration 异常

### （一） 函数必须接收一个可迭代对象参数，每次调用的时候，返回可迭代对象的下一个元素。如果所有元素均已经返回过，则抛出StopIteration 异常。


# Traceback (most recent call last):
#  File "./5-next.py", line 11, in <module>
#    print (next(it))
# StopIteration 


# （二）函数可以接收一个可选的default参数，传入default参数后，如果可迭代对象还有元素没有返回，则依次返回其元素值，如果所有元素已经返回，则返回default指定的默认值而不抛出StopIteration 异常。


print ('2 函数可以接收一个可选的default参数')
a = [1, 2, 3, 4]
it = iter(a)
print (next(it,5))
print (next(it,5))
print (next(it,5))
print (next(it,5))
print (next(it,5))
print (next(it,5))

# 三）如果传入第二个参数, 获取最后一个元素之后, 下一次next返回该默认值, 而不会抛出 StopIteration:
print ('3 如果传入第二个参数, 获取最后一个元素之后, 下一次next返回该默认值')
b = [1, 3, 5, 7, 9]
it = iter(b)
while True:
    try:
        x = next(it)
        print (x, end="# ")
    except StopIteration:
        break
print ('\n ---------------')


