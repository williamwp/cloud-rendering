#!/usr/bin/python3

import os


# 1 get the current working directory for python
pwd = os.getcwd()
print (pwd)

# 2 get the operating system for this computer
name = os.name
if name == 'posix':
    print("this is Linux or UNix")
elif name == 'nt':
    print("this is Windows")
else:
    print("this is another system ")

# 3 delete file-test1.txt
os.remove('file-test1.txt')

# 4 file path
# filepath = '/home/wangpeng/tools/cloud-rendering/csv-python/keys'


