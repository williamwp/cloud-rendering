#!/usr/bin/python3

import numpy as np 
import random

data2 = np.loadtxt('data_processing_data2.txt') 
each_sample_count = 1
label_data_unique = np.unique(data2[:, -1])
print(label_data_unique)

sample_data = []
sample_dict = {}

for label_data in label_data_unique:
    sample_list = []
    for data_tmp in data2:
        if data_tmp[-1] == label_data:
            sample_list.append(data_tmp)
            each_sample_data = random.sample(sample_list, each_sample_count)
            sample_data.extend(each_sample_data)
            sample_dict[label_data] = len(each_sample_data)
print (sample_dict)

'''
wangpeng@test-C9Z490-PGW:~/tools/cloud-rendering/csv-python/keys$ ./6-data-csv-read.py 
[0. 1.]
Traceback (most recent call last):
  File "./6-data-csv-read.py", line 19, in <module>
    each_sample_data = random.sample(sample_list, each_sample_count)
  File "/usr/lib/python3.6/random.py", line 320, in sample
    raise ValueError("Sample larger than population or is negative")
ValueError: Sample larger than population or is negative
wangpeng@test-C9Z490-PGW:~/tools/cloud-rendering/csv-python/keys$ vim 6-data-csv-read.py 
wangpeng@test-C9Z490-PGW:~/tools/cloud-rendering/csv-python/keys$ ./6-data-csv-read.py 
[0. 1.]
{0.0: 1, 1.0: 1}


'''
