#!/usr/bin/python3

import pandas as pd
import os

'''
# example 1
reader = pd.read_table('data_processing_data2.txt', sep='\t', iterator=True)
df = reader.get_chunk(10000)
print(df)
'''

file_path1 = os.getcwd()
file_path = file_path1 + 'test2.txt'

# example 2
if os.path.exists(file_path):
    data = pd.read_csv(file_path, sep=',', engine='python', iterator=True)
    chunk_size = 1000000    
    chunks = []
    loop = True
    while loop:
        try:
            chunk = data.get_chunk(chunk_size)
            chunks.append(chunk)
        except StopIteration:
            loop = False
            print('Iteration is stopped ')
        print('Start concat... ')
        filename = 'data_processing_data3.txt'
        df = pd.concat(chunks, ignore_index=True) 
        print('file %s has %d data' %(filename, df.shape[0]))
#        return df
    else:
        print("Doesn't exist such file ")
#        return None

print('test')
