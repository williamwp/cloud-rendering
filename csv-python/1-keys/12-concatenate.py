#!/usr/bin/python3

import pandas as pd
import numpy as np

# -------------------------------------------------------
a1 = np.array([[1,2],[3,4]])
b1 = np.array([[5,6]])
# ValueError: all the input arrays must have same number of dimensions, but the array at index 0 has 2 dimension(s) and the array at index 1 has 1 dimension(s)


c1 = np.concatenate((a1,b1), axis=0)
print(" c1 = np.concatenate((a1,b1), axis=0)   ")
print(c1)
print(" \n \n ")

c2 = np.concatenate((a1,b1.T), axis=1)
print("c2 = np.concatenate((a1,b1.T), axis=1)  ")
print(c2)

print("\n \n  ")
s1 = "hello world   "
s2 = len(s1)
print("the length of s2 is  ")
print(s2)
print("\n \n  ")



# ------------------------------------------------------
print(" def check(answer, guess): \n ")
def check(answer, guess):
    if answer==guess:
        print('you are right \n\n   ')


# -------------------------------------------------------
print("max ")
print("a = max(12, 23, 15, 56)    ")
a = max(12, 23, 15, 56)
print(a)
print("\n\n   ")

print("upper    ")
s3 = 'apple'.upper()
print(" s3 = 'apple'.upper() ")
print(s3)
print("\n\n  ")


# -------------------------------------------------------
print(" reverse ")
s4 = [1,2,3]
s4.reverse()
print("  s4.reverse(s4)  ")
print(s4)
print("\n\n ")

# -------------------------------------------------------
print(" def my_func():  for i in range(1,11):   ")
def my_func():
    for ii in range(1, 11):
        print(ii)

print(" \n\n")







# -------------------------------------------------------
print("\n\n  ")
data1 = pd.DataFrame(
        np.arange(0,16).reshape(4,4),
        columns=list('abcd')
        )
print("data1 is")
print(data1)

data2 = [
        [1,2,3,4],
        [5,6,7,8],
        [1,2,4,5],
        [1,5,7,8]
        ]

print("\n  ")
data2 = pd.DataFrame(data2, columns=['a', 'b', 'c', 'd'])
print(" data2 is")
print(data2, "\n")

d1 = pd.merge(data1, data2, on=['b'])
print(d1)



