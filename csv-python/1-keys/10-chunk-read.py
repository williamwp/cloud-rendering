#!/usr/bin/python3

import pandas as pd

# coding=utf8

# python使用chunk进行大文件的读写
# 有时候我们会拿到一些很大的文本文件，完整读入内存，读入的过程会很慢，甚至可能无法读入内存，或者可以读入内存，但是没法进行进一步的计算.
# 这个时候如果我们不是要进行很复杂的运算，可以使用read_csv提供的chunksize或者iterator参数，来部分读入文件，处理完之后再通过to_csv的mode=’a’，将每部分结果逐步写入文件。


reader = pd.read_csv('test.csv', iterator = True, low_memory = False )
# if low_memory is set to false, pandas will not loop up, you can directly use larger data type to store. 
# 当iterator被设置成True的时候，read_csv就会迭代读取数据，而不会一次性读取。这样就会返回一个TextFileReader的迭代.




loop = True
chunkSize = 100000
chunks = []


while loop:
    try:
        chunk = reader.get_chunk(chunkSize)
        chunks.append(chunk)
    except StopIteration:
        loop = False
        print("stop iteration ! ")
        df = pd.concat(chunks, ignore_index=True)
#  


