#!/usr/bin/python3

num = 1

def fun1():
    global num # define global key word
    print(num)
    num = 121 # redefine variable num value
    print(num)

fun1() # call function func1()
print(num)
