#!/usr/bin/python3
# coding=utf8
import csv

numbers = [[1, 2, 3], [7, 8, 9], [10, 11, 12] ]

f = open('number3.csv', 'w')

with f:

    writer = csv.writer(f)
    writer.writerows(numbers)

# 该代码示例使用writerows()方法将三行数字写入文件。
