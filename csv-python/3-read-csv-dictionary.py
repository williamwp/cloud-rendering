#!/usr/bin/python3
# coding=utf8
import csv

# Python CSV DictReader

# csv.DictReader类的操作类似于常规读取器，但会将读取的信息映射到字典中。 字典的键可以使用fieldnames参数传入，也可以从 CSV 文件的第一行推断出来。

# 该示例使用csv.DictReader从values.csv文件中读取值。
f = open('values.csv', 'r')

with f:
    reader = csv.DictReader(f)

    for row in reader:
        print(row['min'], row['avg'], row['max'] )


# 该行是 Python 字典，我们使用键引用数据
