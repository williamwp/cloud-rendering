#!/usr/bin/python3

# coding=utf6
import csv

f = open('names.csv', 'w')

with f:
# 该示例使用csv.DictWriter将 Python 词典中的值写入 CSV 文件。
    fnames = ['first_name', 'last_name']
    writer = csv.DictWriter(f, fieldnames=fnames)
# 创建了新的csv.DictWriter。 标头名称将传递给fieldnames参数。

    writer.writeheader()
# writeheader()方法将标头写入 CSV 文件。

    writer.writerow({'first_name' : 'John', 'last_name' : 'Smith'})
# Python 字典被写入 CSV 文件中的一行。
    writer.writerow({'first_name' : 'Robert', 'last_name': 'Brown'})
    writer.writerow({'first_name' : 'Julia', "last_name" : 'Griffin'})

# csv.DictWriter类的操作类似于常规编写器，但将 Python 词典映射到 CSV 行。 fieldnames参数是一系列键，这些键标识传递给writerow()方法的字典中的值写入 CSV 文件的顺序。


