#!/usr/bin/python3

import csv

csv.register_dialect("hashes", delimiter="#")
# 该程序使用（# ）字符作为分隔符。 使用csv.writer()方法中的dialect选项指定方言。

f = open('items3.csv', 'w')

with f:
    writer = csv.writer(f, dialect="hashes")
    writer.writerow(("pens", 4))
    writer.writerow(("plates", 2))
    writer.writerow(("bottles", 4))
    writer.writerow(("cups", 1))



