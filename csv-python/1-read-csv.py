#!/usr/bin/python3

import csv
# to use the python CSV python module, we import CSV

f = open('numbers.csv', 'r')

with f:

    reader = csv.reader(f)

    for row in reader:
        for e in row:
            print(e)

# we open numbers.csv and try to read the content of csv.
# when we get object reader, travel all of the data through two loops.



