#!/bin/bash

start_time=$(date +%s)
sleep 1

ELF=./a12

# begin to execute the nvprof metrics 
if false; then
sudo nvprof -m         l2_tex_write_transactions  $ELF
sudo nvprof -m                     flop_count_hp  $ELF
sudo nvprof -m                 flop_count_hp_add  $ELF
sudo nvprof -m                 flop_count_hp_mul  $ELF
sudo nvprof -m                 flop_count_hp_fma  $ELF
sudo nvprof -m                        inst_fp_16  $ELF
sudo nvprof -m                               ipc  $ELF
sudo nvprof -m                        issued_ipc  $ELF
sudo nvprof -m            issue_slot_utilization  $ELF
sudo nvprof -m          eligible_warps_per_cycle  $ELF
sudo nvprof -m                shared_utilization  $ELF
fi

sudo nvprof -m                    l2_utilization  $ELF

if false; then
sudo nvprof -m                   tex_utilization  $ELF
sudo nvprof -m               ldst_fu_utilization  $ELF
sudo nvprof -m                 cf_fu_utilization  $ELF
sudo nvprof -m                tex_fu_utilization  $ELF
sudo nvprof -m            special_fu_utilization  $ELF
fi

sudo nvprof -m     half_precision_fu_utilization  $ELF
#sudo nvprof -m   single_precision_fu_utilization  $ELF
#sudo nvprof -m   double_precision_fu_utilization  $ELF
sudo nvprof -m                flop_hp_efficiency  $ELF
sudo nvprof -m                flop_sp_efficiency  $ELF
sudo nvprof -m                flop_dp_efficiency  $ELF
#sudo nvprof -m           sysmem_read_utilization  $ELF
#sudo nvprof -m          sysmem_write_utilization  $ELF
sudo nvprof -m       pcie_total_data_transmitted  $ELF
#sudo nvprof -m          pcie_total_data_received  $ELF
sudo nvprof -m        inst_executed_global_loads  $ELF
sudo nvprof -m         inst_executed_local_loads  $ELF
sudo nvprof -m        inst_executed_shared_loads  $ELF
#sudo nvprof -m       inst_executed_surface_loads  $ELF
sudo nvprof -m       inst_executed_global_stores  $ELF
sudo nvprof -m        inst_executed_local_stores  $ELF
sudo nvprof -m       inst_executed_shared_stores  $ELF
#sudo nvprof -m      inst_executed_surface_stores  $ELF
sudo nvprof -m      inst_executed_global_atomics  $ELF
sudo nvprof -m   inst_executed_global_reductions  $ELF
sudo nvprof -m     inst_executed_surface_atomics  $ELF
sudo nvprof -m  inst_executed_surface_reductions  $ELF
sudo nvprof -m      inst_executed_shared_atomics  $ELF
sudo nvprof -m             inst_executed_tex_ops  $ELF


sudo nvprof -m              l2_global_load_bytes  $ELF
sudo nvprof -m               l2_local_load_bytes  $ELF
#sudo nvprof -m             l2_surface_load_bytes  $ELF
sudo nvprof -m                   dram_read_bytes  $ELF
sudo nvprof -m                  dram_write_bytes  $ELF

sudo nvprof -m       l2_local_global_store_bytes  $ELF

if false; then
sudo nvprof -m         l2_global_reduction_bytes  $ELF
sudo nvprof -m      l2_global_atomic_store_bytes  $ELF
sudo nvprof -m            l2_surface_store_bytes  $ELF
sudo nvprof -m        l2_surface_reduction_bytes  $ELF
sudo nvprof -m     l2_surface_atomic_store_bytes  $ELF
sudo nvprof -m              global_load_requests  $ELF
sudo nvprof -m               local_load_requests  $ELF
sudo nvprof -m             surface_load_requests  $ELF
sudo nvprof -m             global_store_requests  $ELF
sudo nvprof -m              local_store_requests  $ELF
sudo nvprof -m            surface_store_requests  $ELF
sudo nvprof -m            global_atomic_requests  $ELF
sudo nvprof -m         global_reduction_requests  $ELF
sudo nvprof -m           surface_atomic_requests  $ELF
sudo nvprof -m        surface_reduction_requests  $ELF
sudo nvprof -m                 sysmem_read_bytes  $ELF
sudo nvprof -m                sysmem_write_bytes  $ELF
fi

sudo nvprof -m                   l2_tex_hit_rate  $ELF
#3sudo nvprof -m             texture_load_requests  $ELF

end_time=$(date +%s)
cost_time=$[ $end_time-$start_time ]
echo "Cost time is $(($cost_time/60)) min $(($cost_time%60)) s"
