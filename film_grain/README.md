# film grain rendering 

The 'bin/film_grain_rendering_main' program reads an input image, and adds film grain noise/texture to this image. Only PNG or TIFF (black and white or colour) images are handled.

# Build
 
sudo apt-get install libpng
sudo apt-get install libtiff

make

mv 2-li-bai.png bin/

# Usage

./film_grain_rendering_main 2-li-bai.png film-grain-li-bai.png 

