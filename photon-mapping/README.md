# photon-mapping

Progressive Photon Mapping with CUDA

![PPM](images/progressive.png)

# Build

cd source

nvcc cudaPPM.cu -o photon-mapping  

# Usage  

./photon-mapping
