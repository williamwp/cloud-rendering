                         OctaneBench 2020.1.5
                        ======================

OctaneBench allows you to benchmark your GPU using OctaneRender.
The idea is to provide a level playing field by making sure that everybody
uses the same version and the same scenes and settings. Without these
constraints, benchmark results can vary a lot and become meaningless.

You can download OctaneBench from
  http://octane.otoy.com/octanebench/
and you can see the submitted results on
  http://octane.otoy.com/octanebench/results.php

OctaneBench comes in two flavours: As an archive that contains a demo version of
OctaneRender Standalone which has been modified to directly launch the benchmark
script. And as an archive that can be uncompressed into a OctaneRender Standalone
directory of version 2020 or lower. The second flavor is recommended only for
experienced users.


System Requirements
--------------------
  
The hardware and software requirements to run OctaneBench are the same as for
OctaneRender Standalone 2020.1.5. You can find details in the OctaneRender FAQ on
  http://home.otoy.com/render/octane-render/faqs/


OctaneBench Archive
--------------------

This is as easy as it gets: Just download the archive for the operating system
you are using, and unpack it into a directory of your choice. These are the
installation steps - depending on the platform you are using:

Windows:

Unpacking the archive "OctaneBench_2020_1_5_win" will create a new folder
"OctaneBench_2020_1_5_win", which contains the OctaneRender demo application, that
directly runs the benchmark script.

Linux:

Unpacking the archive "OctaneBench_2020_1_5_linux" will create a new folder
"OctaneBench_2020_1_5_linux", which contains the OctaneRender demo application,
that directly runs the benchmark script.

Mac OSX:

Opening the archive "OctaneBench_2020_1_5_macos" will show you the bundle
"OctaneBench 2020.1.5" you can either copy it into the application folder or just
run it from there.


OctaneBench Data Archive
-------------------------

The OctaneBench data archive contains only the resource files required to run
OctaneBench and two scripts to launch OctaneRender Standalone on Windows or
Linux. Since OctaneRender Standalone is a bundle on Mac OSX, this method
doesn't work there.

To set things up, uncompress the ZIP archive into the directory, where the
OctaneRender application binary can be found and then just run the script
"_run_benchmark.bat" (if you are on Windows) or "run_benchmark.sh" (if you are
on Linux).


Using OctaneBench
------------------

The usage is straight forward and consists of three steps:

1) Choose if you want to benchmark with or without RTX enabled.

2) Enable the GPUs you want to benchmark.

3) Click "Run" and wait until the benchmark run has finished.

4) Marvel at the statistics, which you can then upload or save as a text / CSV
   file.
   PLEASE NOTE:
     To make the OctaneBench results comparable, the upload is only allowed
     on for official OctaneBench releases.
     To avoid multiple uploads of the same result, the upload button will be
     deactivated after a successfuk upload and will only be activated after a
     new benchmark run.

In case you are wondering: The score is calculated from the measured speed
(Ms/s or mega samples per second) relative to the speed we measured for a
GTX 980 for version 3.08.2. The ratio is weighted by the approximate usage of
the various kernels and then added up.

Please keep in mind that people are not only interested in multi-GPU performance
but also in the performance of single GPUs. Therefor it makes sense to not only
measure the whole system, but also the individual GPUs your system has.


Using OctaneBench via Command Line
-----------------------------------

You can also integrate OctaneBench into some automated testing system by running
it via command line with the following syntax:

  octane  --benchmark  [-g <GPU index>]*  [-a <file>]  [--no-gui]

-g <GPU index>: Defines the index of a GPU that should be used during the run
                (beginning with 0). You can define multiple GPUs by giving
                multiple -g arguments.
                
-a <file>:      If set, the benchmark will start immediately. When finished, the
                result will be written into the specified file and OctaneBench
                will be closed.

--no-gui:       If provided, no GUI will be created. If no file is specified with
                -a, then the results will be saved to octanebench-results.csv in 
                the user home directory.


OctaneBench Scenes
-------------------

The four scenes used in the benchmark were kindly provided by:

Julio Cayetaño (Idea)
Jürgen Aleksejev (ATV)
Enrico Cerica (Box)
Julia Lynen (Interior)

A big thanks to these artists as well as to all the other artists that competed
in the benchmark scene competition back then (see
http://render.otoy.com/forum/viewforum.php?f=62).


Happy Benchmarking!
Your OTOY NZ Team :)
