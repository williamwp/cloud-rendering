# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CUDA"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CUDA
  "/home/wangpeng/cloud-rendering/160c/src/Kernel.cu" "/home/wangpeng/cloud-rendering/160c/build/CMakeFiles/raytracing.dir/src/Kernel.cu.o"
  "/home/wangpeng/cloud-rendering/160c/src/RayTracer.cu" "/home/wangpeng/cloud-rendering/160c/build/CMakeFiles/raytracing.dir/src/RayTracer.cu.o"
  )
set(CMAKE_CUDA_COMPILER_ID "NVIDIA")

# The include file search paths:
set(CMAKE_CUDA_TARGET_INCLUDE_PATH
  "/home/chenjiuyi/usr_chenjiuyi/local/cuda-10.2/include"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/wangpeng/cloud-rendering/160c/src/Display.cpp" "/home/wangpeng/cloud-rendering/160c/build/CMakeFiles/raytracing.dir/src/Display.cpp.o"
  "/home/wangpeng/cloud-rendering/160c/src/utils/file.cpp" "/home/wangpeng/cloud-rendering/160c/build/CMakeFiles/raytracing.dir/src/utils/file.cpp.o"
  "/home/wangpeng/cloud-rendering/160c/src/utils/logger.cpp" "/home/wangpeng/cloud-rendering/160c/build/CMakeFiles/raytracing.dir/src/utils/logger.cpp.o"
  "/home/wangpeng/cloud-rendering/160c/src/utils/png.cpp" "/home/wangpeng/cloud-rendering/160c/build/CMakeFiles/raytracing.dir/src/utils/png.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/chenjiuyi/usr_chenjiuyi/local/cuda-10.2/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
