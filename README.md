# RenderBench

RenderBench is a benchmark for GPU rendering programs, including rasterization, ray tracing, photo mapping and so on, which is written in CUDA.

**Rendering Arithmatic Supported**

| name    | description             | status                    |
| ------- | ---------------- | ----------------------- |
| ray tracing    | info         | :ballot_box_with_check: |
| path tracing  | info     | :ballot_box_with_check: |
| fractal | info | :ballot_box_with_check: |
| photon-mapping  | info     | :ballot_box_with_check: |


[动画演示1](https://gitlab.com/williamwp/cloud-rendering/-/blob/main/%E5%8A%A8%E7%94%BB.gif)
