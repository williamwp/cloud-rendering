Importing ../assets/objs/chess_pawn/pawn.obj
Done : 36352 triangles
Importing ../assets/objs/chess_pawn/ground.obj
Done : 2 triangles
==PROF== Connected to process 21641 (/home/test/tools/cloud-rendering/20c/LiSA-Path-Tracing-Renderer-master/out/LiSA)
Starting rendering on 10 pass(es) and 3 sample(s)...
==PROF== Profiling "trace" - 1: 0%....50%....100% - 14 passes
==PROF== Profiling "trace" - 2: 0%....50%....100% - 14 passes
==PROF== Profiling "trace" - 3: 0%....50%....100% - 14 passes
==PROF== Profiling "trace" - 4: 0%....50%....100% - 14 passes
==PROF== Profiling "trace" - 5: 0%....50%....100% - 14 passes
==PROF== Profiling "trace" - 6: 0%....50%....100% - 14 passes
==PROF== Profiling "trace" - 7: 0%....50%....100% - 14 passes
==PROF== Profiling "trace" - 8: 0%....50%....100% - 14 passes
==PROF== Profiling "trace" - 9: 0%....50%....100% - 14 passes
==PROF== Profiling "trace" - 10: 0%....50%....100% - 14 passes
Redering finished in 19.263 minutes.
Exporting finished.
==PROF== Disconnected from process 21641
    ---------------------------------------------------------------------- --------------- ------------------------------

  trace(glm::mat<(int)4, (int)4, float, (glm::qualifier)0>, glm::vec<(int)3, float, (glm::qualifier)0>, glm::vec<(int)2, float, (glm::qualifier)0>, glm::vec<(int)3, float, (glm::qualifier)0> *, Material *, Sphere *, Triangle *, glm::vec<(int)3, float, (glm::qualifier)0> *, glm::vec<(int)3, float, (glm::qualifier)0> *, int, int, int, glm::vec<(int)2, float, (glm::qualifier)0>, int, int), 2022-Jun-20 11:40:59, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__bytes_read.sum                                                             Gbyte                           1.19
    dram__bytes_read.sum.per_second                                           Mbyte/second                         457.60
    dram__bytes_write.sum                                                            Mbyte                           9.22
    dram__bytes_write.sum.per_second                                          Mbyte/second                           3.54
    dram__sectors_read.sum                                                          sector                     37,264,648
    dram__sectors_write.sum                                                         sector                        288,233
    l1tex__f_tex2sm_cycles_active.avg.pct_of_peak_sustained_elapsed                      %                              0
    l1tex__lsu_writeback_active.avg.pct_of_peak_sustained_active                         %                          19.65
    l1tex__lsu_writeback_active.sum                                                  cycle                 46,047,747,198
    l1tex__t_bytes_pipe_lsu_mem_global_op_ld.sum.per_second                   Gbyte/second                         565.46
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum                                     Mbyte                          36.50
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum.per_second                   Mbyte/second                          14.00
    l1tex__t_bytes_pipe_lsu_mem_local_op_ld.sum.per_second                     byte/second                              0
    l1tex__t_requests_pipe_lsu_mem_global_op_atom.sum                              request                              0
    l1tex__t_requests_pipe_lsu_mem_global_op_ld.sum                                request                 46,047,511,046
    l1tex__t_requests_pipe_lsu_mem_global_op_red.sum                               request                              0
    l1tex__t_requests_pipe_lsu_mem_global_op_st.sum                                request                         89,856
    l1tex__t_requests_pipe_lsu_mem_local_op_ld.sum                                 request                              0
    l1tex__t_requests_pipe_lsu_mem_local_op_st.sum                                 request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_atom.sum                             request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_ld.sum                               request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_red.sum                              request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_st.sum                               request                              0
    l1tex__t_requests_pipe_tex_mem_texture.sum                                     request                              0
    l1tex__t_sector_hit_rate.pct                                                         %                          99.13
    l1tex__t_sectors_pipe_lsu_mem_global_op.sum                                                                   (!) n/a
    l1tex__t_sectors_pipe_lsu_mem_global_op_ld.sum                                  sector                 46,048,552,112
    l1tex__t_sectors_pipe_lsu_mem_global_op_lookup_hit.sum                                                        (!) n/a
    l1tex__t_sectors_pipe_lsu_mem_local_op_ld.sum                                   sector                              0
    l1tex__t_sectors_pipe_lsu_mem_local_op_st.sum                                   sector                              0
    l1tex__t_sectors_pipe_lsu_mem_local_op_st.sum.per_second                 sector/second                              0
    l1tex__t_set_accesses_pipe_lsu_mem_global_op_atom.sum                                                               0
    l1tex__t_set_accesses_pipe_lsu_mem_global_op_red.sum                                                                0
    l1tex__t_set_accesses_pipe_tex_mem_surface_op_atom.sum                                                              0
    l1tex__t_set_accesses_pipe_tex_mem_surface_op_red.sum                                                               0
    l1tex__tex_writeback_active.avg.pct_of_peak_sustained_active                         %                              0
    l1tex__texin_sm2tex_req_cycles_active.avg.pct_of_peak_sustained_elapse               %                           0.00
    d                                                                                                                    
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_global_op_ld.sum                    Gbyte                          12.84
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_global_op_st.sum                    Kbyte                         230.43
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_surface_op_st.sum                                                (!) n/a
    lts__t_bytes_equiv_l1sectormiss_pipe_tex_mem_surface_op_ld.sum                    byte                              0
    lts__t_sector_hit_rate.pct                                                           %                          91.87
    lts__t_sector_op_read_hit_rate.pct                                                   %                          89.26
    lts__t_sector_op_write_hit_rate.pct                                                  %                         100.48
    lts__t_sectors.avg.pct_of_peak_sustained_elapsed                                     %                           0.30
    lts__t_sectors_aperture_sysmem_op_read.sum                                      sector                              0
    lts__t_sectors_aperture_sysmem_op_read.sum.per_second                    sector/second                              0
    lts__t_sectors_aperture_sysmem_op_write.sum                                     sector                          2,961
    lts__t_sectors_aperture_sysmem_op_write.sum.per_second                  sector/msecond                           1.14
    lts__t_sectors_op_read.sum                                                      sector                    454,823,995
    lts__t_sectors_op_read.sum.per_second                                   sector/usecond                         174.53
    lts__t_sectors_op_write.sum                                                     sector                        440,383
    lts__t_sectors_op_write.sum.per_second                                  sector/msecond                         168.99
    lts__t_sectors_srcunit_l1_op_atom.sum                                           sector                              0
    lts__t_sectors_srcunit_l1_op_atom.sum.per_second                         sector/second                              0
    lts__t_sectors_srcunit_tex_op_read.sum                                          sector                    400,969,157
    lts__t_sectors_srcunit_tex_op_read.sum.per_second                       sector/usecond                         153.87
    sm__mio2rf_writeback_active.avg.pct_of_peak_sustained_elapsed                        %                           9.55
    sm__pipe_tensor_cycles_active.avg.pct_of_peak_sustained_active                       %                              0
    sm__warps_active.avg.pct_of_peak_sustained_active                                    %                          39.96
    smsp__average_inst_executed_per_warp.ratio                                   inst/warp                  13,703,364.16
    smsp__cycles_active.avg.pct_of_peak_sustained_elapsed                                %                          96.75
    smsp__inst_executed.avg.per_cycle_active                                    inst/cycle                           0.44
    smsp__inst_executed.sum                                                           inst                410,443,163,331
    smsp__inst_executed_op_global_atom.sum                                                                        (!) n/a
    smsp__inst_executed_op_global_ld.sum                                              inst                 46,047,511,046
    smsp__inst_executed_op_global_st.sum                                              inst                         89,856
    smsp__inst_executed_op_local_ld.sum                                               inst                              0
    smsp__inst_executed_op_shared_atom.sum                                            inst                              0
    smsp__inst_executed_op_shared_atom_dot_alu.sum                                    inst                              0
    smsp__inst_executed_op_shared_atom_dot_cas.sum                                    inst                              0
    smsp__inst_executed_op_shared_ld.avg.pct_of_peak_sustained_elapsed                   %                              0
    smsp__inst_executed_op_shared_ld.sum                                              inst                              0
    smsp__inst_executed_op_shared_ld.sum.per_second                            inst/second                              0
    smsp__inst_executed_op_shared_st.avg.pct_of_peak_sustained_elapsed                   %                              0
    smsp__inst_executed_op_shared_st.sum                                              inst                              0
    smsp__inst_executed_op_shared_st.sum.per_second                            inst/second                              0
    smsp__inst_executed_op_surface_atom.sum                                           inst                              0
    smsp__inst_executed_op_surface_ld.sum                                             inst                              0
    smsp__inst_executed_op_surface_red.sum                                            inst                              0
    smsp__inst_executed_op_surface_st.sum                                             inst                              0
    smsp__inst_executed_op_texture.sum                                                inst                              0
    smsp__inst_executed_pipe_adu.sum                                                  inst                        119,808
    smsp__inst_executed_pipe_cbu.sum                                                  inst                 30,973,801,923
    smsp__inst_executed_pipe_fp16.sum                                                 inst                              0
    smsp__inst_executed_pipe_fp64.avg.pct_of_peak_sustained_active                       %                           0.00
    smsp__inst_executed_pipe_lsu.avg.pct_of_peak_sustained_active                        %                           9.88
    smsp__inst_executed_pipe_tex.avg.pct_of_peak_sustained_active                        %                              0
    smsp__inst_issued.avg.per_cycle_active                                      inst/cycle                           0.44
    smsp__inst_issued.sum                                                             inst                410,443,792,502
    smsp__issue_active.avg.pct_of_peak_sustained_active                                  %                          44.01
    smsp__sass_thread_inst_executed_op_control_pred_on.sum                            inst              1,036,256,919,270
    smsp__sass_thread_inst_executed_op_dadd_pred_on.sum                               inst                      9,414,018
    smsp__sass_thread_inst_executed_op_dfma_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_dmul_pred_on.sum                               inst                      9,414,018
    smsp__sass_thread_inst_executed_op_fadd_pred_on.sum                               inst              1,087,793,723,353
    smsp__sass_thread_inst_executed_op_ffma_pred_on.sum                               inst                976,390,257,888
    smsp__sass_thread_inst_executed_op_fmul_pred_on.sum                               inst                657,230,515,080
    smsp__sass_thread_inst_executed_op_fp16_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fp32_pred_on.sum                               inst              3,562,468,636,726
    smsp__sass_thread_inst_executed_op_fp64_pred_on.sum                               inst                     18,828,036
    smsp__sass_thread_inst_executed_op_hadd_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_hfma_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_hmul_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_integer_pred_on.sum                            inst              2,647,042,953,039
    smsp__sass_thread_inst_executed_op_inter_thread_communication_pred_on.            inst                              0
    sum                                                                                                                  
    smsp__sass_thread_inst_executed_op_memory_pred_on.sum                             inst              1,348,427,657,269
    smsp__sass_thread_inst_executed_op_misc_pred_on.sum                               inst              1,460,070,683,940
    smsp__sass_thread_inst_executed_op_mufu_pred_on.sum                                                           (!) n/a
    smsp__thread_inst_executed_per_inst_executed.pct                                     %                          90.26
    smsp__thread_inst_executed_per_inst_executed.ratio                                                              28.88
    smsp__warp_issue_stalled_drain_miss_per_warp_active.pct                                                       (!) n/a
    smsp__warp_issue_stalled_imc_miss_per_warp_active.pct                                %                           0.00
    smsp__warp_issue_stalled_long_scoreboard_miss_per_warp_active.pct                                             (!) n/a
    smsp__warp_issue_stalled_misc_membar_per_warp_active.pct                                                      (!) n/a
    smsp__warp_issue_stalled_misc_mio_throttle_per_warp_active.pct                                                (!) n/a
    smsp__warp_issue_stalled_misc_miss_per_warp_active.pct                                                        (!) n/a
    smsp__warp_issue_stalled_misc_sleeping_per_warp_active.pct                                                    (!) n/a
    smsp__warp_issue_stalled_misc_tex_throttle_per_warp_active.pct                                                (!) n/a
    smsp__warp_issue_stalled_no_instruction_miss_per_warp_active.pct                                              (!) n/a
    smsp__warp_issue_stalled_short_scoreboard_miss_per_warp_active.pct                                            (!) n/a
    smsp__warps_eligible.sum.per_cycle_active                                         warp                         208.91
    ---------------------------------------------------------------------- --------------- ------------------------------


