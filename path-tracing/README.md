# Path Tracing Renderer

A path tracing rendering engine developped in C++ using CUDA 11 and GLM, which runs in multiple CUDA threads and uses the Monte-Carlo rendering technique.

You can see some examples of rendered images [here](#Some-images).

# Build

cd build

make

# Usage

./LiSA scene_file.rto nb_passes output_file.ppm [nb_sample=3]

