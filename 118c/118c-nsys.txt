wangpeng@amax:~/tools/renderbench/118c$  /usr/local/cuda-11.2/nsight-systems-2020.4.3/target-linux-x64/nsys   profile  --stats=true  ./a.out 
Warning: LBR backtrace method is not supported on this platform. DWARF backtrace method will be used.
Collecting data...
Rendering Image: 10x10
Tempo de Execução: 0.0488558 segundos
Processing events...
Capturing symbol files...
Saving temporary "/tmp/nsys-report-aa89-1937-f0c8-1f6e.qdstrm" file to disk...
Creating final output files...

Processing [==============================================================100%]
Saved report file to "/tmp/nsys-report-aa89-1937-f0c8-1f6e.qdrep"
Exporting 2912 events: [==================================================100%]

Exported successfully to
/tmp/nsys-report-aa89-1937-f0c8-1f6e.sqlite


CUDA API Statistics:

 Time(%)  Total Time (ns)  Num Calls    Average     Minimum    Maximum           Name         
 -------  ---------------  ---------  -----------  ---------  ---------  ---------------------
    67.5        306039472          1  306039472.0  306039472  306039472  cudaMallocManaged    
    18.0         81619445          1   81619445.0   81619445   81619445  cudaDeviceReset      
    10.5         47579025          3   15859675.0      40733   25096536  cudaDeviceSynchronize
     3.6         16528660          4    4132165.0       5248   16138678  cudaFree             
     0.3          1184100          3     394700.0      11487    1154283  cudaMalloc           
     0.1           651480          3     217160.0      12900     618387  cudaLaunchKernel     
     0.0             2825          1       2825.0       2825       2825  cuCtxSynchronize     



CUDA Kernel Statistics:

 Time(%)  Total Time (ns)  Instances   Average    Minimum   Maximum                            Name                         
 -------  ---------------  ---------  ----------  --------  --------  ------------------------------------------------------
    39.4         25091544          1  25091544.0  25091544  25091544  render(vec3*, int, int, int, hitable**, camera**)     
    35.3         22445370          1  22445370.0  22445370  22445370  create_scene(hitable**, hitable**, camera**, int, int)
    25.3         16128212          1  16128212.0  16128212  16128212  free_scene(hitable**, hitable**, camera**)            



CUDA Memory Operation Statistics (by time):

 Time(%)  Total Time (ns)  Operations  Average  Minimum  Maximum              Operation            
 -------  ---------------  ----------  -------  -------  -------  ---------------------------------
   100.0             9087           2   4543.5     2399     6688  [CUDA Unified Memory memcpy DtoH]



CUDA Memory Operation Statistics (by size in KiB):

 Total   Operations  Average  Minimum  Maximum              Operation            
 ------  ----------  -------  -------  -------  ---------------------------------
 64.000           2   32.000    4.000   60.000  [CUDA Unified Memory memcpy DtoH]



Operating System Runtime API Statistics:

 Time(%)  Total Time (ns)  Num Calls   Average    Minimum   Maximum        Name     
 -------  ---------------  ---------  ----------  -------  ---------  --------------
    68.1        669372480         53  12629669.4     1638  100209679  poll          
    21.0        206298489       1557    132497.4     1023   28888086  ioctl         
     9.4         92136181         39   2362466.2    14860   21369130  sem_timedwait 
     0.7          6620170        120     55168.1     3169    1136073  mmap64        
     0.4          4132972         26    158960.5     1300     964700  mmap          
     0.2          1940611         39     49759.3     1729    1719180  fopen         
     0.1           864848        101      8562.9     3077      34784  open64        
     0.1           654833         35     18709.5     2454     425341  munmap        
     0.0           248552        122      2037.3     1206      11692  write         
     0.0           185983          4     46495.8    42211      49953  pthread_create
     0.0           180473          6     30078.8     2500     124223  fread         
     0.0           145726         32      4553.9     1033      67448  fclose        
     0.0           140548          1    140548.0   140548     140548  pthread_join  
     0.0            61817          1     61817.0    61817      61817  fgets         
     0.0            59580          1     59580.0    59580      59580  fopen64       
     0.0            54949         20      2747.5     1134       4117  read          
     0.0            37434          5      7486.8     3958      13070  open          
     0.0            17249          3      5749.7     1139      13363  fwrite        
     0.0            16739          3      5579.7     1584       8557  fgetc         
     0.0             9639          1      9639.0     9639       9639  pipe2         
     0.0             8690          2      4345.0     3533       5157  socket        
     0.0             7107          3      2369.0     1380       4132  fcntl         
     0.0             6864          1      6864.0     6864       6864  connect       
     0.0             5655          1      5655.0     5655       5655  putc          
     0.0             1552          1      1552.0     1552       1552  bind          

Report file moved to "/home/wangpeng/tools/renderbench/118c/report1.qdrep"
Report file moved to "/home/wangpeng/tools/renderbench/118c/report1.sqlite"
wangpeng@amax:~/tools/renderbench/118c$ 
