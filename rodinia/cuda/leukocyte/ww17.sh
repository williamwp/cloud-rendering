#!/bin/bash

start_time=$(date +%s)
sleep 1

ELF=./run

# begin to execute the nvprof metrics 
sudo nvprof -m              l2_read_transactions  --profile-child-processes  $ELF
sudo nvprof -m             l2_write_transactions  --profile-child-processes  $ELF
sudo nvprof -m            dram_read_transactions  --profile-child-processes  $ELF
sudo nvprof -m           dram_write_transactions  --profile-child-processes  $ELF
sudo nvprof -m                   global_hit_rate  --profile-child-processes  $ELF
sudo nvprof -m                    local_hit_rate  --profile-child-processes  $ELF
sudo nvprof -m          gld_requested_throughput  --profile-child-processes  $ELF
sudo nvprof -m          gst_requested_throughput  --profile-child-processes  $ELF
sudo nvprof -m                    gld_throughput  --profile-child-processes  $ELF
sudo nvprof -m                    gst_throughput  --profile-child-processes  $ELF
sudo nvprof -m             local_memory_overhead  --profile-child-processes  $ELF
sudo nvprof -m                tex_cache_hit_rate  --profile-child-processes  $ELF
sudo nvprof -m              l2_tex_read_hit_rate  --profile-child-processes  $ELF
sudo nvprof -m             l2_tex_write_hit_rate  --profile-child-processes  $ELF
sudo nvprof -m              dram_read_throughput  --profile-child-processes  $ELF
sudo nvprof -m             dram_write_throughput  --profile-child-processes  $ELF
sudo nvprof -m              tex_cache_throughput  --profile-child-processes  $ELF
sudo nvprof -m            l2_tex_read_throughput  --profile-child-processes  $ELF
sudo nvprof -m           l2_tex_write_throughput  --profile-child-processes  $ELF
sudo nvprof -m                l2_read_throughput  --profile-child-processes  $ELF
sudo nvprof -m               l2_write_throughput  --profile-child-processes  $ELF
sudo nvprof -m            sysmem_read_throughput  --profile-child-processes  $ELF
sudo nvprof -m           sysmem_write_throughput  --profile-child-processes  $ELF
sudo nvprof -m             local_load_throughput  --profile-child-processes  $ELF
sudo nvprof -m            local_store_throughput  --profile-child-processes  $ELF
sudo nvprof -m            shared_load_throughput  --profile-child-processes  $ELF
sudo nvprof -m           shared_store_throughput  --profile-child-processes  $ELF
sudo nvprof -m                    gld_efficiency  --profile-child-processes  $ELF
sudo nvprof -m                    gst_efficiency  --profile-child-processes  $ELF
sudo nvprof -m            tex_cache_transactions  --profile-child-processes  $ELF


end_time=$(date +%s)
cost_time=$[ $end_time-$start_time ]
echo "Cost time is $(($cost_time/60)) min $(($cost_time%60)) s"
