==PROF== Connected to process 21527 (/home/wangpeng/tools/advanced_c/cloud-rendering/rodinia/cuda/dwt2d/dwt2d)
==PROF== Profiling "c_CopySrcToComponents" - 1: 0%....50%....100% - 5 passes
==PROF== Profiling "fdwt53Kernel" - 2: 0%....50%....100% - 5 passes
==PROF== Profiling "fdwt53Kernel" - 3: 0%....50%....100% - 5 passes
==PROF== Profiling "fdwt53Kernel" - 4: 0%....50%....100% - 5 passes
==PROF== Profiling "fdwt53Kernel" - 5: 0%....50%....100% - 5 passes
==PROF== Profiling "fdwt53Kernel" - 6: 0%....50%....100% - 5 passes
==PROF== Profiling "fdwt53Kernel" - 7: 0%....50%....100% - 5 passes
==PROF== Profiling "fdwt53Kernel" - 8: 0%....50%....100% - 5 passes
==PROF== Profiling "fdwt53Kernel" - 9: 0%....50%....100% - 5 passes
==PROF== Profiling "fdwt53Kernel" - 10: 0%....50%....100% - 5 passes
Using device 0: NVIDIA A100 80GB PCIe
Source file:		192.bmp
 Dimensions:		192x192
 Components count:	3
 Bit depth:		8
 DWT levels:		3
 Forward transform:	1
 9/7 transform:		0
Loading ipnput: ../../data/dwt2d/192.bmp
precteno 110592, inputsize 110592

*** 3 stages of 2D forward DWT:

 sliding steps = 2 , gx = 3 , gy = 12 
fdwt53Kernel in launchFDWT53Kernel has finished
 sliding steps = 1 , gx = 2 , gy = 12 
fdwt53Kernel in launchFDWT53Kernel has finished
 sliding steps = 1 , gx = 1 , gy = 6 
fdwt53Kernel in launchFDWT53Kernel has finished
*** 3 stages of 2D forward DWT:

 sliding steps = 2 , gx = 3 , gy = 12 
fdwt53Kernel in launchFDWT53Kernel has finished
 sliding steps = 1 , gx = 2 , gy = 12 
fdwt53Kernel in launchFDWT53Kernel has finished
 sliding steps = 1 , gx = 1 , gy = 6 
fdwt53Kernel in launchFDWT53Kernel has finished
*** 3 stages of 2D forward DWT:

 sliding steps = 2 , gx = 3 , gy = 12 
fdwt53Kernel in launchFDWT53Kernel has finished
 sliding steps = 1 , gx = 2 , gy = 12 
fdwt53Kernel in launchFDWT53Kernel has finished
 sliding steps = 1 , gx = 1 , gy = 6 
fdwt53Kernel in launchFDWT53Kernel has finished
Writing to 192.bmp.dwt.r (192 x 192)

Writing to 192.bmp.dwt.g (192 x 192)

Writing to 192.bmp.dwt.b (192 x 192)
==PROF== Disconnected from process 21527
[21527] dwt2d@127.0.0.1
  void c_CopySrcToComponents<int>(int*, int*, int*, unsigned char*, int), 2022-Apr-12 07:32:01, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__throughput.avg.pct_of_peak_sustained_elapsed                                   %                           6.11
    l1tex__lsu_writeback_active.sum                                                  cycle                           5472
    l1tex__t_bytes_pipe_lsu_mem_global_op_ld.sum.per_second                   Gbyte/second                          24.86
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum                                     Kbyte                         442.37
    l1tex__t_sectors_pipe_lsu_mem_global_op_ld.sum                                  sector                           3456
    smsp__sass_thread_inst_executed_op_fp16_pred_on.avg                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.max                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.min                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.sum                               inst                              0
    sm__warps_active.avg.pct_of_peak_sustained_active                                    %                          16.16
    smsp__inst_executed.avg.per_cycle_active                                    inst/cycle                           0.04
    smsp__inst_executed_op_shared_ld.avg.pct_of_peak_sustained_elapsed                   %                           0.17
    smsp__inst_executed_op_shared_st.avg.pct_of_peak_sustained_elapsed                   %                           0.04
    smsp__inst_executed_pipe_fp16.sum                                                 inst                              0
    smsp__inst_executed_pipe_fp64.avg.pct_of_peak_sustained_active                       %                              0
    smsp__issue_active.avg.pct_of_peak_sustained_active                                  %                           5.13
    smsp__pipe_fma_cycles_active.avg.pct_of_peak_sustained_active                        %                           1.60
    smsp__sass_thread_inst_executed_op_fp32_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fp64_pred_on.sum                               inst                              0
    smsp__thread_inst_executed_per_inst_executed.pct                                     %                            100
    smsp__thread_inst_executed_per_inst_executed.ratio                                                                 32
    ---------------------------------------------------------------------- --------------- ------------------------------

  void dwt_cuda::fdwt53Kernel<64, 8>(int const*, int*, int, int, int), 2022-Apr-12 07:32:02, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__throughput.avg.pct_of_peak_sustained_elapsed                                   %                           1.76
    l1tex__lsu_writeback_active.sum                                                  cycle                           7670
    l1tex__t_bytes_pipe_lsu_mem_global_op_ld.sum.per_second                   Gbyte/second                          28.98
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum                                     Kbyte                         147.46
    l1tex__t_sectors_pipe_lsu_mem_global_op_ld.sum                                  sector                           6840
    smsp__sass_thread_inst_executed_op_fp16_pred_on.avg                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.max                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.min                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.sum                               inst                              0
    sm__warps_active.avg.pct_of_peak_sustained_active                                    %                           3.12
    smsp__inst_executed.avg.per_cycle_active                                    inst/cycle                           0.12
    smsp__inst_executed_op_shared_ld.avg.pct_of_peak_sustained_elapsed                   %                           0.16
    smsp__inst_executed_op_shared_st.avg.pct_of_peak_sustained_elapsed                   %                           0.11
    smsp__inst_executed_pipe_fp16.sum                                                 inst                              0
    smsp__inst_executed_pipe_fp64.avg.pct_of_peak_sustained_active                       %                              0
    smsp__issue_active.avg.pct_of_peak_sustained_active                                  %                          12.07
    smsp__pipe_fma_cycles_active.avg.pct_of_peak_sustained_active                        %                           6.96
    smsp__sass_thread_inst_executed_op_fp32_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fp64_pred_on.sum                               inst                              0
    smsp__thread_inst_executed_per_inst_executed.pct                                     %                          86.43
    smsp__thread_inst_executed_per_inst_executed.ratio                                                              27.66
    ---------------------------------------------------------------------- --------------- ------------------------------

  void dwt_cuda::fdwt53Kernel<64, 8>(int const*, int*, int, int, int), 2022-Apr-12 07:32:03, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__throughput.avg.pct_of_peak_sustained_elapsed                                   %                           1.08
    l1tex__lsu_writeback_active.sum                                                  cycle                           2622
    l1tex__t_bytes_pipe_lsu_mem_global_op_ld.sum.per_second                   Gbyte/second                          16.30
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum                                     Kbyte                          36.86
    l1tex__t_sectors_pipe_lsu_mem_global_op_ld.sum                                  sector                           2640
    smsp__sass_thread_inst_executed_op_fp16_pred_on.avg                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.max                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.min                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.sum                               inst                              0
    sm__warps_active.avg.pct_of_peak_sustained_active                                    %                           3.12
    smsp__inst_executed.avg.per_cycle_active                                    inst/cycle                           0.12
    smsp__inst_executed_op_shared_ld.avg.pct_of_peak_sustained_elapsed                   %                           0.08
    smsp__inst_executed_op_shared_st.avg.pct_of_peak_sustained_elapsed                   %                           0.05
    smsp__inst_executed_pipe_fp16.sum                                                 inst                              0
    smsp__inst_executed_pipe_fp64.avg.pct_of_peak_sustained_active                       %                              0
    smsp__issue_active.avg.pct_of_peak_sustained_active                                  %                          12.50
    smsp__pipe_fma_cycles_active.avg.pct_of_peak_sustained_active                        %                           7.55
    smsp__sass_thread_inst_executed_op_fp32_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fp64_pred_on.sum                               inst                              0
    smsp__thread_inst_executed_per_inst_executed.pct                                     %                          85.06
    smsp__thread_inst_executed_per_inst_executed.ratio                                                              27.22
    ---------------------------------------------------------------------- --------------- ------------------------------

  void dwt_cuda::fdwt53Kernel<64, 8>(int const*, int*, int, int, int), 2022-Apr-12 07:32:04, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__throughput.avg.pct_of_peak_sustained_elapsed                                   %                           0.58
    l1tex__lsu_writeback_active.sum                                                  cycle                            633
    l1tex__t_bytes_pipe_lsu_mem_global_op_ld.sum.per_second                   Gbyte/second                           3.70
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum                                     Kbyte                           9.22
    l1tex__t_sectors_pipe_lsu_mem_global_op_ld.sum                                  sector                            592
    smsp__sass_thread_inst_executed_op_fp16_pred_on.avg                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.max                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.min                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.sum                               inst                              0
    sm__warps_active.avg.pct_of_peak_sustained_active                                    %                           3.12
    smsp__inst_executed.avg.per_cycle_active                                    inst/cycle                           0.12
    smsp__inst_executed_op_shared_ld.avg.pct_of_peak_sustained_elapsed                   %                           0.02
    smsp__inst_executed_op_shared_st.avg.pct_of_peak_sustained_elapsed                   %                           0.01
    smsp__inst_executed_pipe_fp16.sum                                                 inst                              0
    smsp__inst_executed_pipe_fp64.avg.pct_of_peak_sustained_active                       %                              0
    smsp__issue_active.avg.pct_of_peak_sustained_active                                  %                          12.79
    smsp__pipe_fma_cycles_active.avg.pct_of_peak_sustained_active                        %                           8.20
    smsp__sass_thread_inst_executed_op_fp32_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fp64_pred_on.sum                               inst                              0
    smsp__thread_inst_executed_per_inst_executed.pct                                     %                          85.17
    smsp__thread_inst_executed_per_inst_executed.ratio                                                              27.26
    ---------------------------------------------------------------------- --------------- ------------------------------

  void dwt_cuda::fdwt53Kernel<64, 8>(int const*, int*, int, int, int), 2022-Apr-12 07:32:05, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__throughput.avg.pct_of_peak_sustained_elapsed                                   %                           2.06
    l1tex__lsu_writeback_active.sum                                                  cycle                           7669
    l1tex__t_bytes_pipe_lsu_mem_global_op_ld.sum.per_second                   Gbyte/second                             30
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum                                     Kbyte                         147.46
    l1tex__t_sectors_pipe_lsu_mem_global_op_ld.sum                                  sector                           6840
    smsp__sass_thread_inst_executed_op_fp16_pred_on.avg                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.max                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.min                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.sum                               inst                              0
    sm__warps_active.avg.pct_of_peak_sustained_active                                    %                           3.12
    smsp__inst_executed.avg.per_cycle_active                                    inst/cycle                           0.12
    smsp__inst_executed_op_shared_ld.avg.pct_of_peak_sustained_elapsed                   %                           0.16
    smsp__inst_executed_op_shared_st.avg.pct_of_peak_sustained_elapsed                   %                           0.11
    smsp__inst_executed_pipe_fp16.sum                                                 inst                              0
    smsp__inst_executed_pipe_fp64.avg.pct_of_peak_sustained_active                       %                              0
    smsp__issue_active.avg.pct_of_peak_sustained_active                                  %                          11.93
    smsp__pipe_fma_cycles_active.avg.pct_of_peak_sustained_active                        %                           6.88
    smsp__sass_thread_inst_executed_op_fp32_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fp64_pred_on.sum                               inst                              0
    smsp__thread_inst_executed_per_inst_executed.pct                                     %                          86.43
    smsp__thread_inst_executed_per_inst_executed.ratio                                                              27.66
    ---------------------------------------------------------------------- --------------- ------------------------------

  void dwt_cuda::fdwt53Kernel<64, 8>(int const*, int*, int, int, int), 2022-Apr-12 07:32:06, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__throughput.avg.pct_of_peak_sustained_elapsed                                   %                           0.95
    l1tex__lsu_writeback_active.sum                                                  cycle                           2621
    l1tex__t_bytes_pipe_lsu_mem_global_op_ld.sum.per_second                   Gbyte/second                          16.30
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum                                     Kbyte                          36.86
    l1tex__t_sectors_pipe_lsu_mem_global_op_ld.sum                                  sector                           2640
    smsp__sass_thread_inst_executed_op_fp16_pred_on.avg                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.max                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.min                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.sum                               inst                              0
    sm__warps_active.avg.pct_of_peak_sustained_active                                    %                           3.12
    smsp__inst_executed.avg.per_cycle_active                                    inst/cycle                           0.12
    smsp__inst_executed_op_shared_ld.avg.pct_of_peak_sustained_elapsed                   %                           0.08
    smsp__inst_executed_op_shared_st.avg.pct_of_peak_sustained_elapsed                   %                           0.05
    smsp__inst_executed_pipe_fp16.sum                                                 inst                              0
    smsp__inst_executed_pipe_fp64.avg.pct_of_peak_sustained_active                       %                              0
    smsp__issue_active.avg.pct_of_peak_sustained_active                                  %                          12.41
    smsp__pipe_fma_cycles_active.avg.pct_of_peak_sustained_active                        %                           7.50
    smsp__sass_thread_inst_executed_op_fp32_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fp64_pred_on.sum                               inst                              0
    smsp__thread_inst_executed_per_inst_executed.pct                                     %                          85.06
    smsp__thread_inst_executed_per_inst_executed.ratio                                                              27.22
    ---------------------------------------------------------------------- --------------- ------------------------------

  void dwt_cuda::fdwt53Kernel<64, 8>(int const*, int*, int, int, int), 2022-Apr-12 07:32:07, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__throughput.avg.pct_of_peak_sustained_elapsed                                   %                           0.60
    l1tex__lsu_writeback_active.sum                                                  cycle                            633
    l1tex__t_bytes_pipe_lsu_mem_global_op_ld.sum.per_second                   Gbyte/second                           3.79
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum                                     Kbyte                           9.22
    l1tex__t_sectors_pipe_lsu_mem_global_op_ld.sum                                  sector                            592
    smsp__sass_thread_inst_executed_op_fp16_pred_on.avg                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.max                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.min                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.sum                               inst                              0
    sm__warps_active.avg.pct_of_peak_sustained_active                                    %                           3.12
    smsp__inst_executed.avg.per_cycle_active                                    inst/cycle                           0.13
    smsp__inst_executed_op_shared_ld.avg.pct_of_peak_sustained_elapsed                   %                           0.02
    smsp__inst_executed_op_shared_st.avg.pct_of_peak_sustained_elapsed                   %                           0.01
    smsp__inst_executed_pipe_fp16.sum                                                 inst                              0
    smsp__inst_executed_pipe_fp64.avg.pct_of_peak_sustained_active                       %                              0
    smsp__issue_active.avg.pct_of_peak_sustained_active                                  %                          13.10
    smsp__pipe_fma_cycles_active.avg.pct_of_peak_sustained_active                        %                           8.40
    smsp__sass_thread_inst_executed_op_fp32_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fp64_pred_on.sum                               inst                              0
    smsp__thread_inst_executed_per_inst_executed.pct                                     %                          85.17
    smsp__thread_inst_executed_per_inst_executed.ratio                                                              27.26
    ---------------------------------------------------------------------- --------------- ------------------------------

  void dwt_cuda::fdwt53Kernel<64, 8>(int const*, int*, int, int, int), 2022-Apr-12 07:32:08, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__throughput.avg.pct_of_peak_sustained_elapsed                                   %                           2.36
    l1tex__lsu_writeback_active.sum                                                  cycle                           7670
    l1tex__t_bytes_pipe_lsu_mem_global_op_ld.sum.per_second                   Gbyte/second                          28.86
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum                                     Kbyte                         147.46
    l1tex__t_sectors_pipe_lsu_mem_global_op_ld.sum                                  sector                           6840
    smsp__sass_thread_inst_executed_op_fp16_pred_on.avg                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.max                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.min                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.sum                               inst                              0
    sm__warps_active.avg.pct_of_peak_sustained_active                                    %                           3.12
    smsp__inst_executed.avg.per_cycle_active                                    inst/cycle                           0.11
    smsp__inst_executed_op_shared_ld.avg.pct_of_peak_sustained_elapsed                   %                           0.16
    smsp__inst_executed_op_shared_st.avg.pct_of_peak_sustained_elapsed                   %                           0.11
    smsp__inst_executed_pipe_fp16.sum                                                 inst                              0
    smsp__inst_executed_pipe_fp64.avg.pct_of_peak_sustained_active                       %                              0
    smsp__issue_active.avg.pct_of_peak_sustained_active                                  %                          11.40
    smsp__pipe_fma_cycles_active.avg.pct_of_peak_sustained_active                        %                           6.58
    smsp__sass_thread_inst_executed_op_fp32_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fp64_pred_on.sum                               inst                              0
    smsp__thread_inst_executed_per_inst_executed.pct                                     %                          86.43
    smsp__thread_inst_executed_per_inst_executed.ratio                                                              27.66
    ---------------------------------------------------------------------- --------------- ------------------------------

  void dwt_cuda::fdwt53Kernel<64, 8>(int const*, int*, int, int, int), 2022-Apr-12 07:32:09, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__throughput.avg.pct_of_peak_sustained_elapsed                                   %                           0.61
    l1tex__lsu_writeback_active.sum                                                  cycle                           2622
    l1tex__t_bytes_pipe_lsu_mem_global_op_ld.sum.per_second                   Gbyte/second                          16.10
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum                                     Kbyte                          36.86
    l1tex__t_sectors_pipe_lsu_mem_global_op_ld.sum                                  sector                           2640
    smsp__sass_thread_inst_executed_op_fp16_pred_on.avg                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.max                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.min                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.sum                               inst                              0
    sm__warps_active.avg.pct_of_peak_sustained_active                                    %                           3.12
    smsp__inst_executed.avg.per_cycle_active                                    inst/cycle                           0.12
    smsp__inst_executed_op_shared_ld.avg.pct_of_peak_sustained_elapsed                   %                           0.08
    smsp__inst_executed_op_shared_st.avg.pct_of_peak_sustained_elapsed                   %                           0.05
    smsp__inst_executed_pipe_fp16.sum                                                 inst                              0
    smsp__inst_executed_pipe_fp64.avg.pct_of_peak_sustained_active                       %                              0
    smsp__issue_active.avg.pct_of_peak_sustained_active                                  %                          12.50
    smsp__pipe_fma_cycles_active.avg.pct_of_peak_sustained_active                        %                           7.55
    smsp__sass_thread_inst_executed_op_fp32_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fp64_pred_on.sum                               inst                              0
    smsp__thread_inst_executed_per_inst_executed.pct                                     %                          85.06
    smsp__thread_inst_executed_per_inst_executed.ratio                                                              27.22
    ---------------------------------------------------------------------- --------------- ------------------------------

  void dwt_cuda::fdwt53Kernel<64, 8>(int const*, int*, int, int, int), 2022-Apr-12 07:32:10, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__throughput.avg.pct_of_peak_sustained_elapsed                                   %                           0.66
    l1tex__lsu_writeback_active.sum                                                  cycle                            633
    l1tex__t_bytes_pipe_lsu_mem_global_op_ld.sum.per_second                   Gbyte/second                           3.70
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum                                     Kbyte                           9.22
    l1tex__t_sectors_pipe_lsu_mem_global_op_ld.sum                                  sector                            592
    smsp__sass_thread_inst_executed_op_fp16_pred_on.avg                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.max                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.min                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.sum                               inst                              0
    sm__warps_active.avg.pct_of_peak_sustained_active                                    %                           3.12
    smsp__inst_executed.avg.per_cycle_active                                    inst/cycle                           0.13
    smsp__inst_executed_op_shared_ld.avg.pct_of_peak_sustained_elapsed                   %                           0.02
    smsp__inst_executed_op_shared_st.avg.pct_of_peak_sustained_elapsed                   %                           0.01
    smsp__inst_executed_pipe_fp16.sum                                                 inst                              0
    smsp__inst_executed_pipe_fp64.avg.pct_of_peak_sustained_active                       %                              0
    smsp__issue_active.avg.pct_of_peak_sustained_active                                  %                          12.96
    smsp__pipe_fma_cycles_active.avg.pct_of_peak_sustained_active                        %                           8.31
    smsp__sass_thread_inst_executed_op_fp32_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fp64_pred_on.sum                               inst                              0
    smsp__thread_inst_executed_per_inst_executed.pct                                     %                          85.17
    smsp__thread_inst_executed_per_inst_executed.ratio                                                              27.26
    ---------------------------------------------------------------------- --------------- ------------------------------

