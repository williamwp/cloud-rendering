#!/bin/bash

start_time=$(date +%s)
sleep 1
 
echo "Find the register reuse numbers in nn-all-record-reg-vals.txt"

#sed -n "1, 32768p" 182-all-mem-printf.txt | grep -c  "reuse"

aa1=$(sed -n '1, 32768p' nn-all-record-reg-vals.txt | grep -c  "reuse")
echo "1#  The first interval 1 --> 32768 =  $aa1"
for(( i = 15; i < 34; i++ ));
do
j=$[$i+1]
k=$[$i-13]
# $((2**15)) is the begining part of the interval
aa2=$(sed -n "$((2**i)), $((2**j))p" nn-all-record-reg-vals.txt | grep -c  "reuse")
echo "$k#  The middle interval from $((2**i)) to $((2**j)) = $aa2 "
done

aa3=$(sed -n '17179869184, $p' nn-all-record-reg-vals.txt | grep -c  "reuse")
echo "21#  The end interval 17179869184 --> END = $aa3"

echo "Done Successfully !"

if false; then
a=$(sed -n "1, 10099p" 182-all-mem-printf.txt)
grep -c  "reuse" $a
fi
# wrong code:"grep: unrecognized option '-------------'
# Usage: grep [OPTION]... PATTERN [FILE]...
# Try 'grep --help' for more information  "

