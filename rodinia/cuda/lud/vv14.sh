#!/bin/bash

start_time=$(date +%s)
sleep 1

ELF=./run

# begin to execute the nvprof metrics 
sudo nvprof -m     inst_per_warp   --profile-child-processes  $ELF
sudo nvprof -m                 branch_efficiency   --profile-child-processes  $ELF
sudo nvprof -m         warp_execution_efficiency   --profile-child-processes  $ELF
sudo nvprof -m  warp_nonpred_execution_efficiency   --profile-child-processes  $ELF
sudo nvprof -m              inst_replay_overhead   --profile-child-processes  $ELF
sudo nvprof -m  shared_load_transactions_per_request   --profile-child-processes  $ELF
sudo nvprof -m  shared_store_transactions_per_request   --profile-child-processes  $ELF
sudo nvprof -m  local_load_transactions_per_request   --profile-child-processes  $ELF
sudo nvprof -m  local_store_transactions_per_request   --profile-child-processes  $ELF
sudo nvprof -m                achieved_occupancy   --profile-child-processes  $ELF
sudo nvprof -m          eligible_warps_per_cycle   --profile-child-processes  $ELF
sudo nvprof -m             unique_warps_launched   --profile-child-processes  $ELF
sudo nvprof -m         stall_sync     --profile-child-processes  $ELF
sudo nvprof -m       stall_not_selected       --profile-child-processes  $ELF
sudo nvprof -m        sm_efficiency      --profile-child-processes  $ELF
sudo nvprof -m          achieved_occupancy    --profile-child-processes  $ELF


end_time=$(date +%s)
cost_time=$[ $end_time-$start_time ]
echo "Cost time is $(($cost_time/60)) min $(($cost_time%60)) s"
