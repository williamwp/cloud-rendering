#!/bin/bash

ISSET_ROUGHSETS_KDD99_TEST_DATA_FOLDER=$(echo /home/wangpeng/tools/renderbench/machine-learning/roughsets-base-master/tests/datasets/KDD99)

if [ "$ISSET_ROUGHSETS_KDD99_TEST_DATA_FOLDER" == "" ]
then
  echo "Variable ROUGHSETS_KDD99_TEST_DATA_FOLDER not set"
  exit
else
  echo "ROUGHSETS_KDD99_TEST_DATA_FOLDER = /home/wangpeng/tools/renderbench/machine-learning/roughsets-base-master/tests/datasets/KDD99"
fi

echo "Building ..."
rm -rf dist
python -m build
pip install dist/*.whl --force-reinstall

echo "Running unit tests ..."
python -m unittest discover -s $PWD/tests -t $PWD/tests

echo "Running flake8 tests ..."
flake8

echo "Running tox tests ..."
#tox

