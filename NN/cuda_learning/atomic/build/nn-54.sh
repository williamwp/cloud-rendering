#!/bin/bash

start_time=$(date +%s)
sleep 1

ELF=./sum

# begin to execute the nvprof metrics 
sudo nvprof -m      gld_transactions_per_request  --profile-child-processes $ELF
sudo nvprof -m      gst_transactions_per_request  --profile-child-processes $ELF
sudo nvprof -m         shared_store_transactions  --profile-child-processes $ELF
sudo nvprof -m          shared_load_transactions  --profile-child-processes $ELF
sudo nvprof -m           local_load_transactions  --profile-child-processes $ELF
sudo nvprof -m          local_store_transactions  --profile-child-processes $ELF
sudo nvprof -m                  gld_transactions  --profile-child-processes $ELF
sudo nvprof -m                  gst_transactions  --profile-child-processes $ELF
sudo nvprof -m          sysmem_read_transactions  --profile-child-processes $ELF
sudo nvprof -m         sysmem_write_transactions  --profile-child-processes $ELF
sudo nvprof -m              l2_read_transactions  --profile-child-processes $ELF
sudo nvprof -m             l2_write_transactions  --profile-child-processes $ELF
sudo nvprof -m            dram_read_transactions  --profile-child-processes $ELF
sudo nvprof -m           dram_write_transactions  --profile-child-processes $ELF
sudo nvprof -m                     flop_count_dp  --profile-child-processes $ELF
sudo nvprof -m                 flop_count_dp_add  --profile-child-processes $ELF
sudo nvprof -m                 flop_count_dp_fma  --profile-child-processes $ELF
sudo nvprof -m                 flop_count_dp_mul  --profile-child-processes $ELF
sudo nvprof -m                     flop_count_sp  --profile-child-processes $ELF
sudo nvprof -m                 flop_count_sp_add  --profile-child-processes $ELF
sudo nvprof -m                 flop_count_sp_fma  --profile-child-processes $ELF
sudo nvprof -m                 flop_count_sp_mul  --profile-child-processes $ELF
sudo nvprof -m             flop_count_sp_special  --profile-child-processes $ELF
sudo nvprof -m                     inst_executed  --profile-child-processes $ELF
sudo nvprof -m                       inst_issued  --profile-child-processes $ELF
sudo nvprof -m                  dram_utilization  --profile-child-processes $ELF
sudo nvprof -m                sysmem_utilization  --profile-child-processes $ELF
sudo nvprof -m                  stall_inst_fetch  --profile-child-processes $ELF
sudo nvprof -m             stall_exec_dependency  --profile-child-processes $ELF
sudo nvprof -m           stall_memory_dependency  --profile-child-processes $ELF
sudo nvprof -m                     stall_texture  --profile-child-processes $ELF
sudo nvprof -m                       stall_other  --profile-child-processes $ELF
sudo nvprof -m  stall_constant_memory_dependency  --profile-child-processes $ELF
sudo nvprof -m                   stall_pipe_busy  --profile-child-processes $ELF
sudo nvprof -m                 shared_efficiency  --profile-child-processes $ELF
sudo nvprof -m                        inst_fp_32  --profile-child-processes $ELF
sudo nvprof -m                        inst_fp_64  --profile-child-processes $ELF
sudo nvprof -m                      inst_integer  --profile-child-processes $ELF
sudo nvprof -m                  inst_bit_convert  --profile-child-processes $ELF
sudo nvprof -m                      inst_control  --profile-child-processes $ELF
sudo nvprof -m                inst_compute_ld_st  --profile-child-processes $ELF
sudo nvprof -m                         inst_misc  --profile-child-processes $ELF
sudo nvprof -m   inst_inter_thread_communication  --profile-child-processes $ELF
sudo nvprof -m                       issue_slots  --profile-child-processes $ELF
sudo nvprof -m                         cf_issued  --profile-child-processes $ELF
sudo nvprof -m                       cf_executed  --profile-child-processes $ELF
sudo nvprof -m                       ldst_issued  --profile-child-processes $ELF
sudo nvprof -m                     ldst_executed  --profile-child-processes $ELF
sudo nvprof -m               atomic_transactions  --profile-child-processes $ELF
sudo nvprof -m   atomic_transactions_per_request  --profile-child-processes $ELF
sudo nvprof -m              l2_atomic_throughput  --profile-child-processes $ELF
sudo nvprof -m            l2_atomic_transactions  --profile-child-processes $ELF
sudo nvprof -m          l2_tex_read_transactions  --profile-child-processes $ELF
sudo nvprof -m             stall_memory_throttle  --profile-child-processes $ELF



end_time=$(date +%s)
cost_time=$[ $end_time-$start_time ]
echo "Cost time is $(($cost_time/60)) min $(($cost_time%60)) s"
