#!/bin/bash

start_time=$(date +%s)
sleep 1

ELF=./adder
sudo nvprof -m       pcie_total_data_transmitted  --profile-child-processes  $ELF
sudo nvprof -m          pcie_total_data_received  --profile-child-processes  $ELF

# begin to execute the nvprof metrics 
if false; then
sudo nvprof -m         l2_tex_write_transactions  --profile-child-processes  $ELF
sudo nvprof -m                     flop_count_hp  --profile-child-processes  $ELF
sudo nvprof -m                 flop_count_hp_add  --profile-child-processes  $ELF
sudo nvprof -m                 flop_count_hp_mul  --profile-child-processes  $ELF
sudo nvprof -m                 flop_count_hp_fma  --profile-child-processes  $ELF
sudo nvprof -m                        inst_fp_16  --profile-child-processes  $ELF
sudo nvprof -m                               ipc  --profile-child-processes  $ELF
sudo nvprof -m                        issued_ipc  --profile-child-processes  $ELF
sudo nvprof -m            issue_slot_utilization  --profile-child-processes  $ELF
sudo nvprof -m          eligible_warps_per_cycle  --profile-child-processes  $ELF
sudo nvprof -m                shared_utilization  --profile-child-processes  $ELF
sudo nvprof -m                    l2_utilization  --profile-child-processes  $ELF
sudo nvprof -m                   tex_utilization  --profile-child-processes  $ELF
sudo nvprof -m               ldst_fu_utilization  --profile-child-processes  $ELF
sudo nvprof -m                 cf_fu_utilization  --profile-child-processes  $ELF
sudo nvprof -m                tex_fu_utilization  --profile-child-processes  $ELF
sudo nvprof -m            special_fu_utilization  --profile-child-processes  $ELF
sudo nvprof -m     half_precision_fu_utilization  --profile-child-processes  $ELF
sudo nvprof -m   single_precision_fu_utilization  --profile-child-processes  $ELF
sudo nvprof -m   double_precision_fu_utilization  --profile-child-processes  $ELF
sudo nvprof -m                flop_hp_efficiency  --profile-child-processes  $ELF
sudo nvprof -m                flop_sp_efficiency  --profile-child-processes  $ELF
sudo nvprof -m                flop_dp_efficiency  --profile-child-processes  $ELF
sudo nvprof -m           sysmem_read_utilization  --profile-child-processes  $ELF
sudo nvprof -m          sysmem_write_utilization  --profile-child-processes  $ELF
sudo nvprof -m       pcie_total_data_transmitted  --profile-child-processes  $ELF
sudo nvprof -m          pcie_total_data_received  --profile-child-processes  $ELF
sudo nvprof -m        inst_executed_global_loads  --profile-child-processes  $ELF
sudo nvprof -m         inst_executed_local_loads  --profile-child-processes  $ELF
sudo nvprof -m        inst_executed_shared_loads  --profile-child-processes  $ELF
sudo nvprof -m       inst_executed_surface_loads  --profile-child-processes  $ELF
sudo nvprof -m       inst_executed_global_stores  --profile-child-processes  $ELF
sudo nvprof -m        inst_executed_local_stores  --profile-child-processes  $ELF
sudo nvprof -m       inst_executed_shared_stores  --profile-child-processes  $ELF
sudo nvprof -m      inst_executed_surface_stores  --profile-child-processes  $ELF
sudo nvprof -m      inst_executed_global_atomics  --profile-child-processes  $ELF
sudo nvprof -m   inst_executed_global_reductions  --profile-child-processes  $ELF
sudo nvprof -m     inst_executed_surface_atomics  --profile-child-processes  $ELF
sudo nvprof -m  inst_executed_surface_reductions  --profile-child-processes  $ELF
sudo nvprof -m      inst_executed_shared_atomics  --profile-child-processes  $ELF
sudo nvprof -m             inst_executed_tex_ops  --profile-child-processes  $ELF
sudo nvprof -m              l2_global_load_bytes  --profile-child-processes  $ELF
sudo nvprof -m               l2_local_load_bytes  --profile-child-processes  $ELF
sudo nvprof -m             l2_surface_load_bytes  --profile-child-processes  $ELF
sudo nvprof -m                   dram_read_bytes  --profile-child-processes  $ELF
sudo nvprof -m                  dram_write_bytes  --profile-child-processes  $ELF
sudo nvprof -m       l2_local_global_store_bytes  --profile-child-processes  $ELF
sudo nvprof -m         l2_global_reduction_bytes  --profile-child-processes  $ELF
sudo nvprof -m      l2_global_atomic_store_bytes  --profile-child-processes  $ELF
sudo nvprof -m            l2_surface_store_bytes  --profile-child-processes  $ELF
sudo nvprof -m        l2_surface_reduction_bytes  --profile-child-processes  $ELF
sudo nvprof -m     l2_surface_atomic_store_bytes  --profile-child-processes  $ELF
sudo nvprof -m              global_load_requests  --profile-child-processes  $ELF
sudo nvprof -m               local_load_requests  --profile-child-processes  $ELF
sudo nvprof -m             surface_load_requests  --profile-child-processes  $ELF
sudo nvprof -m             global_store_requests  --profile-child-processes  $ELF
sudo nvprof -m              local_store_requests  --profile-child-processes  $ELF
sudo nvprof -m            surface_store_requests  --profile-child-processes  $ELF
sudo nvprof -m            global_atomic_requests  --profile-child-processes  $ELF
sudo nvprof -m         global_reduction_requests  --profile-child-processes  $ELF
sudo nvprof -m           surface_atomic_requests  --profile-child-processes  $ELF
sudo nvprof -m        surface_reduction_requests  --profile-child-processes  $ELF
sudo nvprof -m                 sysmem_read_bytes  --profile-child-processes  $ELF
sudo nvprof -m                sysmem_write_bytes  --profile-child-processes  $ELF
sudo nvprof -m                   l2_tex_hit_rate  --profile-child-processes  $ELF
sudo nvprof -m             texture_load_requests  --profile-child-processes  $ELF
fi
end_time=$(date +%s)
cost_time=$[ $end_time-$start_time ]
echo "Cost time is $(($cost_time/60)) min $(($cost_time%60)) s"
