#!/bin/bash

start_time=$(date +%s)
sleep 1

ELF=./mallocPitch

# begin to execute the nvprof metrics 
sudo nvprof -m      gld_transactions_per_request  $ELF
sudo nvprof -m      gst_transactions_per_request  $ELF
sudo nvprof -m         shared_store_transactions  $ELF
sudo nvprof -m          shared_load_transactions  $ELF
sudo nvprof -m           local_load_transactions  $ELF
sudo nvprof -m          local_store_transactions  $ELF
sudo nvprof -m                  gld_transactions  $ELF
sudo nvprof -m                  gst_transactions  $ELF
sudo nvprof -m          sysmem_read_transactions  $ELF
sudo nvprof -m         sysmem_write_transactions  $ELF
sudo nvprof -m              l2_read_transactions  $ELF
sudo nvprof -m             l2_write_transactions  $ELF
sudo nvprof -m            dram_read_transactions  $ELF
sudo nvprof -m           dram_write_transactions  $ELF
sudo nvprof -m                     flop_count_dp  $ELF
sudo nvprof -m                 flop_count_dp_add  $ELF
sudo nvprof -m                 flop_count_dp_fma  $ELF
sudo nvprof -m                 flop_count_dp_mul  $ELF
sudo nvprof -m                     flop_count_sp  $ELF
sudo nvprof -m                 flop_count_sp_add  $ELF
sudo nvprof -m                 flop_count_sp_fma  $ELF
sudo nvprof -m                 flop_count_sp_mul  $ELF
sudo nvprof -m             flop_count_sp_special  $ELF
sudo nvprof -m                     inst_executed  $ELF
sudo nvprof -m                       inst_issued  $ELF
sudo nvprof -m                  dram_utilization  $ELF
sudo nvprof -m                sysmem_utilization  $ELF
sudo nvprof -m                  stall_inst_fetch  $ELF
sudo nvprof -m             stall_exec_dependency  $ELF
sudo nvprof -m           stall_memory_dependency  $ELF
sudo nvprof -m                     stall_texture  $ELF
sudo nvprof -m                       stall_other  $ELF
sudo nvprof -m  stall_constant_memory_dependency  $ELF
sudo nvprof -m                   stall_pipe_busy  $ELF
sudo nvprof -m                 shared_efficiency  $ELF
sudo nvprof -m                        inst_fp_32  $ELF
sudo nvprof -m                        inst_fp_64  $ELF
sudo nvprof -m                      inst_integer  $ELF
sudo nvprof -m                  inst_bit_convert  $ELF
sudo nvprof -m                      inst_control  $ELF
sudo nvprof -m                inst_compute_ld_st  $ELF
sudo nvprof -m                         inst_misc  $ELF
sudo nvprof -m   inst_inter_thread_communication  $ELF
sudo nvprof -m                       issue_slots  $ELF
sudo nvprof -m                         cf_issued  $ELF
sudo nvprof -m                       cf_executed  $ELF
sudo nvprof -m                       ldst_issued  $ELF
sudo nvprof -m                     ldst_executed  $ELF
sudo nvprof -m               atomic_transactions  $ELF
sudo nvprof -m   atomic_transactions_per_request  $ELF
sudo nvprof -m              l2_atomic_throughput  $ELF
sudo nvprof -m            l2_atomic_transactions  $ELF
sudo nvprof -m          l2_tex_read_transactions  $ELF
sudo nvprof -m             stall_memory_throttle  $ELF



end_time=$(date +%s)
cost_time=$[ $end_time-$start_time ]
echo "Cost time is $(($cost_time/60)) min $(($cost_time%60)) s"
