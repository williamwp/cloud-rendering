#!/bin/bash

start_time=$(date +%s)
sleep 1

ELF=./Toeplitz

# begin to execute the nvprof metrics 
sudo nvprof -m     inst_per_warp  $ELF
sudo nvprof -m                 branch_efficiency  $ELF
sudo nvprof -m         warp_execution_efficiency  $ELF
sudo nvprof -m  warp_nonpred_execution_efficiency  $ELF
sudo nvprof -m              inst_replay_overhead  $ELF
sudo nvprof -m  shared_load_transactions_per_request  $ELF
sudo nvprof -m  shared_store_transactions_per_request  $ELF
sudo nvprof -m  local_load_transactions_per_request  $ELF
sudo nvprof -m  local_store_transactions_per_request  $ELF
sudo nvprof -m                achieved_occupancy  $ELF
sudo nvprof -m          eligible_warps_per_cycle  $ELF
sudo nvprof -m             unique_warps_launched  $ELF
sudo nvprof -m         stall_sync    $ELF
sudo nvprof -m       stall_not_selected      $ELF
sudo nvprof -m        sm_efficiency     $ELF
sudo nvprof -m          achieved_occupancy   $ELF


end_time=$(date +%s)
cost_time=$[ $end_time-$start_time ]
echo "Cost time is $(($cost_time/60)) min $(($cost_time%60)) s"
