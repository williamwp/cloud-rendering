==PROF== Connected to process 16876 (/home/wangpeng/tools/advanced_c/cloud-rendering/158/raytraceroptimized-master/a.out)
==PROF== Profiling "trace2" - 1: 0%....50%....100% - 5 passes
==PROF== Disconnected from process 16876
[16876] a.out@127.0.0.1
  trace2(Vec3*, Sphere*, int, unsigned int, unsigned int), 2022-Apr-14 03:03:10, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__throughput.avg.pct_of_peak_sustained_elapsed                                   %                           0.59
    l1tex__lsu_writeback_active.sum                                                  cycle                         812476
    l1tex__t_bytes_pipe_lsu_mem_global_op_ld.sum.per_second                   Gbyte/second                          30.74
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum                                     Kbyte                         774.88
    l1tex__t_sectors_pipe_lsu_mem_global_op_ld.sum                                  sector                         757679
    smsp__sass_thread_inst_executed_op_fp16_pred_on.avg                               inst                         432.65
    smsp__sass_thread_inst_executed_op_fp16_pred_on.max                               inst                           4008
    smsp__sass_thread_inst_executed_op_fp16_pred_on.min                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.sum                               inst                         186903
    sm__warps_active.avg.pct_of_peak_sustained_active                                    %                          10.48
    smsp__inst_executed.avg.per_cycle_active                                    inst/cycle                           0.26
    smsp__inst_executed_op_shared_ld.avg.pct_of_peak_sustained_elapsed                   %                              0
    smsp__inst_executed_op_shared_st.avg.pct_of_peak_sustained_elapsed                   %                              0
    smsp__inst_executed_pipe_fp16.sum                                                 inst                              0
    smsp__inst_executed_pipe_fp64.avg.pct_of_peak_sustained_active                       %                           1.98
    smsp__issue_active.avg.pct_of_peak_sustained_active                                  %                          26.44
    smsp__pipe_fma_cycles_active.avg.pct_of_peak_sustained_active                        %                          18.21
    smsp__sass_thread_inst_executed_op_fp32_pred_on.sum                               inst                       28006069
    smsp__sass_thread_inst_executed_op_fp64_pred_on.sum                               inst                        1372527
    smsp__thread_inst_executed_per_inst_executed.pct                                     %                          38.54
    smsp__thread_inst_executed_per_inst_executed.ratio                                                              12.33
    ---------------------------------------------------------------------- --------------- ------------------------------

