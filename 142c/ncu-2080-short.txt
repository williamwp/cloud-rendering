==PROF== Connected to process 14280 (/home/test/tools/cloud-rendering/142c/a.out)
preparing depth cache ...==PROF== Profiling "initRayBuffer_cukern" - 1: 0%....50%....100% - 13 passes
 done
ray marching ...==PROF== Profiling "rtRaymarch_cukern" - 2: 0%....50%....100% - 13 passes
 done
building depth cache ...==PROF== Profiling "renderDepthCache_cukern" - 3: 0%....50%....100% - 13 passes
==PROF== Profiling "initRayBufferWithDepthCache_c..." - 4: 0%....50%....100% - 13 passes
==PROF== Profiling "rtRaymarch_cukern" - 5: 0%....50%....100% - 13 passes
==PROF== Profiling "shaderNormal_cukern" - 6: 0%....50%....100% - 13 passes
 done
depth cache size:108473
.==PROF== Profiling "initRayBufferWithDepthCache_c..." - 7: 0%....50%....100% - 13 passes
==PROF== Profiling "rtRaymarch_cukern" - 8: 0%....50%....100% - 13 passes
==PROF== Profiling "shaderNormal_cukern" - 9: 0%....50%....100% - 13 passes
.==PROF== Profiling "initRayBufferWithDepthCache_c..." - 10: 0%....50%....100% - 13 passes
==PROF== Profiling "rtRaymarch_cukern" - 11: 0%....50%....100% - 13 passes
==PROF== Profiling "shaderNormal_cukern" - 12: 0%....50%....100% - 13 passes
.==PROF== Profiling "initRayBufferWithDepthCache_c..." - 13: 0%....50%....100% - 13 passes
==PROF== Profiling "rtRaymarch_cukern" - 14: 0%....50%....100% - 13 passes
==PROF== Profiling "shaderNormal_cukern" - 15: 0%....50%....100% - 13 passes
.==PROF== Profiling "initRayBufferWithDepthCache_c..." - 16: 0%....50%....100% - 13 passes
==PROF== Profiling "rtRaymarch_cukern" - 17: 0%....50%....100% - 13 passes
==PROF== Profiling "shaderNormal_cukern" - 18: 0%....50%....100% - 13 passes
.==PROF== Profiling "initRayBufferWithDepthCache_c..." - 19: 0%....50%....100% - 13 passes
==PROF== Profiling "rtRaymarch_cukern" - 20: 0%....50%....100% - 13 passes
==PROF== Profiling "shaderNormal_cukern" - 21: 0%....50%....100% - 13 passes
.==PROF== Profiling "initRayBufferWithDepthCache_c..." - 22: 0%....50%....100% - 13 passes
==PROF== Profiling "rtRaymarch_cukern" - 23: 0%....50%....100% - 13 passes
==PROF== Profiling "shaderNormal_cukern" - 24: 0%....50%....100% - 13 passes
.==PROF== Profiling "initRayBufferWithDepthCache_c..." - 25: 0%....50%....100% - 13 passes
==PROF== Profiling "rtRaymarch_cukern" - 26: 0%....50%....100% - 13 passes
==PROF== Profiling "shaderNormal_cukern" - 27: 0%....50%....100% - 13 passes
.==PROF== Profiling "initRayBufferWithDepthCache_c..." - 28: 0%....50%....100% - 13 passes
==PROF== Profiling "rtRaymarch_cukern" - 29: 0%....50%....100% - 13 passes
==PROF== Profiling "shaderNormal_cukern" - 30: 0%....50%....100% - 13 passes
.==PROF== Profiling "initRayBufferWithDepthCache_c..." - 31: 0%....50%....100% - 13 passes
==PROF== Profiling "rtRaymarch_cukern" - 32: 0%....50%....100% - 13 passes
==PROF== Profiling "shaderNormal_cukern" - 33: 0%....50%....100% - 13 passes
.==PROF== Profiling "initRayBufferWithDepthCache_c..." - 34: 0%....50%....100% - 13 passes
    ---------------------------------------------------------------------- --------------- ------------------------------

  renderDepthCache_cukern(RayPtr *, int *, const Ray *, int), 2022-May-18 01:00:15, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    dram__bytes_read.sum                                                             Mbyte                          34.42
    dram__bytes_read.sum.per_second                                           Gbyte/second                          18.21
    dram__bytes_write.sum                                                            Kbyte                         903.20
    dram__bytes_write.sum.per_second                                          Mbyte/second                         477.90
    dram__sectors_read.sum                                                          sector                      1,075,581
    dram__sectors_write.sum                                                         sector                         28,225
    l1tex__f_tex2sm_cycles_active.avg.pct_of_peak_sustained_elapsed                      %                              0
    l1tex__lsu_writeback_active.avg.pct_of_peak_sustained_active                         %                          10.88
    l1tex__lsu_writeback_active.sum                                                  cycle                        277,691
    l1tex__t_bytes_pipe_lsu_mem_global_op_ld.sum.per_second                   Gbyte/second                          37.34
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum                                     Mbyte                           1.91
    l1tex__t_bytes_pipe_lsu_mem_global_op_st.sum.per_second                   Gbyte/second                           1.01
    l1tex__t_bytes_pipe_lsu_mem_local_op_ld.sum.per_second                     byte/second                              0
    l1tex__t_requests_pipe_lsu_mem_global_op_atom.sum                              request                              0
    l1tex__t_requests_pipe_lsu_mem_global_op_ld.sum                                request                         69,258
    l1tex__t_requests_pipe_lsu_mem_global_op_red.sum                               request                              0
    l1tex__t_requests_pipe_lsu_mem_global_op_st.sum                                request                          7,445
    l1tex__t_requests_pipe_lsu_mem_local_op_ld.sum                                 request                              0
    l1tex__t_requests_pipe_lsu_mem_local_op_st.sum                                 request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_atom.sum                             request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_ld.sum                               request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_red.sum                              request                              0
    l1tex__t_requests_pipe_tex_mem_surface_op_st.sum                               request                              0
    l1tex__t_requests_pipe_tex_mem_texture.sum                                     request                              0
    l1tex__t_sector_hit_rate.pct                                                         %                          52.75
    l1tex__t_sectors_pipe_lsu_mem_global_op.sum                                                                   (!) n/a
    l1tex__t_sectors_pipe_lsu_mem_global_op_ld.sum                                  sector                      2,205,625
    l1tex__t_sectors_pipe_lsu_mem_global_op_lookup_hit.sum                                                        (!) n/a
    l1tex__t_sectors_pipe_lsu_mem_local_op_ld.sum                                   sector                              0
    l1tex__t_sectors_pipe_lsu_mem_local_op_st.sum                                   sector                              0
    l1tex__t_sectors_pipe_lsu_mem_local_op_st.sum.per_second                 sector/second                              0
    l1tex__t_set_accesses_pipe_lsu_mem_global_op_atom.sum                                                               0
    l1tex__t_set_accesses_pipe_lsu_mem_global_op_red.sum                                                                0
    l1tex__t_set_accesses_pipe_tex_mem_surface_op_atom.sum                                                              0
    l1tex__t_set_accesses_pipe_tex_mem_surface_op_red.sum                                                               0
    l1tex__tex_writeback_active.avg.pct_of_peak_sustained_active                         %                              0
    l1tex__texin_sm2tex_req_cycles_active.avg.pct_of_peak_sustained_elapse               %                           0.00
    d                                                                                                                    
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_global_op_ld.sum                    Mbyte                          33.56
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_global_op_st.sum                    Kbyte                         694.46
    lts__t_bytes_equiv_l1sectormiss_pipe_lsu_mem_surface_op_st.sum                                                (!) n/a
    lts__t_bytes_equiv_l1sectormiss_pipe_tex_mem_surface_op_ld.sum                    byte                              0
    lts__t_sector_hit_rate.pct                                                           %                           4.85
    lts__t_sector_op_read_hit_rate.pct                                                   %                           0.02
    lts__t_sector_op_write_hit_rate.pct                                                  %                         100.35
    lts__t_sectors.avg.pct_of_peak_sustained_elapsed                                     %                           1.06
    lts__t_sectors_aperture_sysmem_op_read.sum                                      sector                              0
    lts__t_sectors_aperture_sysmem_op_read.sum.per_second                    sector/second                              0
    lts__t_sectors_aperture_sysmem_op_write.sum                                     sector                              7
    lts__t_sectors_aperture_sysmem_op_write.sum.per_second                  sector/msecond                           3.70
    lts__t_sectors_op_read.sum                                                      sector                      1,049,573
    lts__t_sectors_op_read.sum.per_second                                   sector/usecond                         555.34
    lts__t_sectors_op_write.sum                                                     sector                         53,614
    lts__t_sectors_op_write.sum.per_second                                  sector/usecond                          28.37
    lts__t_sectors_srcunit_l1_op_atom.sum                                           sector                              0
    lts__t_sectors_srcunit_l1_op_atom.sum.per_second                         sector/second                              0
    lts__t_sectors_srcunit_tex_op_read.sum                                          sector                      1,048,672
    lts__t_sectors_srcunit_tex_op_read.sum.per_second                       sector/usecond                         554.87
    sm__mio2rf_writeback_active.avg.pct_of_peak_sustained_elapsed                        %                           0.06
    sm__pipe_tensor_cycles_active.avg.pct_of_peak_sustained_active                       %                              0
    sm__warps_active.avg.pct_of_peak_sustained_active                                    %                          99.92
    smsp__average_inst_executed_per_warp.ratio                                   inst/warp                      48,173.09
    smsp__cycles_active.avg.pct_of_peak_sustained_elapsed                                %                           1.48
    smsp__inst_executed.avg.per_cycle_active                                    inst/cycle                           0.15
    smsp__inst_executed.sum                                                           inst                      1,541,539
    smsp__inst_executed_op_global_atom.sum                                                                        (!) n/a
    smsp__inst_executed_op_global_ld.sum                                              inst                         98,304
    smsp__inst_executed_op_global_st.sum                                              inst                         65,537
    smsp__inst_executed_op_local_ld.sum                                               inst                              0
    smsp__inst_executed_op_shared_atom.sum                                            inst                              0
    smsp__inst_executed_op_shared_atom_dot_alu.sum                                    inst                              0
    smsp__inst_executed_op_shared_atom_dot_cas.sum                                    inst                              0
    smsp__inst_executed_op_shared_ld.avg.pct_of_peak_sustained_elapsed                   %                           0.01
    smsp__inst_executed_op_shared_ld.sum                                              inst                         66,560
    smsp__inst_executed_op_shared_ld.sum.per_second                           inst/usecond                          35.22
    smsp__inst_executed_op_shared_st.avg.pct_of_peak_sustained_elapsed                   %                           0.00
    smsp__inst_executed_op_shared_st.sum                                              inst                         33,792
    smsp__inst_executed_op_shared_st.sum.per_second                           inst/usecond                          17.88
    smsp__inst_executed_op_surface_atom.sum                                           inst                              0
    smsp__inst_executed_op_surface_ld.sum                                             inst                              0
    smsp__inst_executed_op_surface_red.sum                                            inst                              0
    smsp__inst_executed_op_surface_st.sum                                             inst                              0
    smsp__inst_executed_op_texture.sum                                                inst                              0
    smsp__inst_executed_pipe_adu.sum                                                  inst                         65,536
    smsp__inst_executed_pipe_cbu.sum                                                  inst                        131,201
    smsp__inst_executed_pipe_fp16.sum                                                 inst                              0
    smsp__inst_executed_pipe_fp64.avg.pct_of_peak_sustained_active                       %                              0
    smsp__inst_executed_pipe_lsu.avg.pct_of_peak_sustained_active                        %                           6.57
    smsp__inst_executed_pipe_tex.avg.pct_of_peak_sustained_active                        %                              0
    smsp__inst_issued.avg.per_cycle_active                                      inst/cycle                           0.15
    smsp__inst_issued.sum                                                             inst                      1,574,435
    smsp__issue_active.avg.pct_of_peak_sustained_active                                  %                          15.45
    smsp__sass_thread_inst_executed_op_control_pred_on.sum                            inst                      6,391,808
    smsp__sass_thread_inst_executed_op_dadd_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_dfma_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_dmul_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fadd_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_ffma_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fmul_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fp16_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_fp32_pred_on.sum                               inst                      2,097,152
    smsp__sass_thread_inst_executed_op_fp64_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_hadd_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_hfma_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_hmul_pred_on.sum                               inst                              0
    smsp__sass_thread_inst_executed_op_integer_pred_on.sum                            inst                     18,730,795
    smsp__sass_thread_inst_executed_op_inter_thread_communication_pred_on.            inst                      1,212,416
    sum                                                                                                                  
    smsp__sass_thread_inst_executed_op_memory_pred_on.sum                             inst                      4,585,260
    smsp__sass_thread_inst_executed_op_misc_pred_on.sum                               inst                      3,181,568
    smsp__sass_thread_inst_executed_op_mufu_pred_on.sum                                                           (!) n/a
    smsp__thread_inst_executed_per_inst_executed.pct                                     %                          87.51
    smsp__thread_inst_executed_per_inst_executed.ratio                                                              28.00
    smsp__warp_issue_stalled_drain_miss_per_warp_active.pct                                                       (!) n/a
    smsp__warp_issue_stalled_imc_miss_per_warp_active.pct                                %                           0.02
    smsp__warp_issue_stalled_long_scoreboard_miss_per_warp_active.pct                                             (!) n/a
    smsp__warp_issue_stalled_misc_membar_per_warp_active.pct                                                      (!) n/a
    smsp__warp_issue_stalled_misc_mio_throttle_per_warp_active.pct                                                (!) n/a
    smsp__warp_issue_stalled_misc_miss_per_warp_active.pct                                                        (!) n/a
    smsp__warp_issue_stalled_misc_sleeping_per_warp_active.pct                                                    (!) n/a
    smsp__warp_issue_stalled_misc_tex_throttle_per_warp_active.pct                                                (!) n/a
    smsp__warp_issue_stalled_no_instruction_miss_per_warp_active.pct                                              (!) n/a
    smsp__warp_issue_stalled_short_scoreboard_miss_per_warp_active.pct                                            (!) n/a
    smsp__warps_eligible.sum.per_cycle_active                                         warp                          80.69
    ---------------------------------------------------------------------- --------------- ------------------------------

  void initRayBufferWithDepthCache_cukern<(bool)1>(Ray *, int, int, RayPtr *, int, int, mat4x4), 2022-May-18 01:00:20, Context 1, Stream 7
    Section: Command line profiler metrics
    ---------------------------------------------------------------------- --------------- ------------------------------
    ---------------------------------------------------------------------- --------------- ------------------------------

